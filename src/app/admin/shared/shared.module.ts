import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {MatIconModule} from "@angular/material/icon";
import {MatFormFieldModule} from "@angular/material/form-field";
import {AutoCompletePatientComponent} from "./auto-complete-patient/auto-complete-patient.component";
import {MatInputModule} from "@angular/material/input";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {ReactiveFormsModule} from "@angular/forms";
import { AutoCompleteDoctorComponent } from './auto-complete-doctor/auto-complete-doctor.component';

@NgModule({
  declarations: [
    AutoCompletePatientComponent,
    AutoCompleteDoctorComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatFormFieldModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
  ],
  exports: [AutoCompletePatientComponent,AutoCompleteDoctorComponent]
})
export class SharedModule {}
