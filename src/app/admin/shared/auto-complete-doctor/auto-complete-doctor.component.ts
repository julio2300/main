import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DataModelSelect } from "../../../shared/DataModelSelect";
import { Patient as Doctor } from "../../doctors/alldoctors/doctors.model";
import { Observable } from "rxjs";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Payment } from "../../payment/allpayment/payment.model";
import { PaymentService } from "../../payment/allpayment/payment.service";
import { HttpClient } from "@angular/common/http";
import { MatSnackBar } from "@angular/material/snack-bar";
import { AdminSettingsService } from "../../../services/AdminSettings.service";
import { map, startWith } from "rxjs/operators";
import { DoctorsService } from 'src/app/services/doctors.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'DevCode-auto-complete-doctor',
  templateUrl: './auto-complete-doctor.component.html',
  styleUrls: ['./auto-complete-doctor.component.sass']
})
export class AutoCompleteDoctorComponent implements OnInit {
  private dataModel: DataModelSelect = {
    limit: 10,
    offset: 0,
    count: 0,
    pageIndex: 0,
    opcion: "S",
    search: '',
  };
  private _doctorService: DoctorsService | null;
  private DataGetPatient: Doctor[] = [];
  public filteredOptions: Observable<Doctor[]>;
  public paymentForm: FormGroup;
  private payment: Payment;
  @Output() pk_doctor = new EventEmitter<Doctor | null>();
  constructor(
    public paymentService: PaymentService,
    private fb: FormBuilder,
    public httpClient: HttpClient,
    public DoctorsService: DoctorsService,
    private snackBar: MatSnackBar,
    private _AdminSettingsService: AdminSettingsService,
  ) {
    this.payment = new Payment({});
    this.paymentForm = this.createContactForm();
  }
  ngOnInit(): void {
    this.loadData();
  }
  private createContactForm(): FormGroup {
    return this.fb.group({
      DoctorName: ['', Validators.required],
    });
  }
  private _filter(value: string): Doctor[] {
    const filterValue = value.toLowerCase();
    return this.DataGetPatient.filter(option => option.doctor_apellidos.toLowerCase().includes(filterValue) || option.doctor_nombre.toLowerCase().includes(filterValue) || option.doctor_cedula.toLowerCase().includes(filterValue));
  }
  public loadData() {
    this._doctorService = new DoctorsService(
      this.httpClient,
      this._AdminSettingsService
    );
    this.GetDataPatiens();
  }
  GetDataPatiens(): void {
    this._doctorService.apiGetDoctors(this.dataModel.opcion, 1, this.dataModel)
      .subscribe((resp) => {
        this.DataGetPatient = resp.data.data;
        this.dataModel.count = Math.ceil(resp.data.conteo / this.dataModel.limit);
        this.filteredOptions = this.paymentForm.get('DoctorName').valueChanges.pipe(
          startWith(''),
          map(value => this._filter(value)),
        );
      });
  }

  ChangePatient(data: Doctor) {
    this.pk_doctor.emit(data);
  }

  ChangeStatus() {
    this.pk_doctor.emit(null);
  }
}
