import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoCompleteDoctorComponent } from './auto-complete-doctor.component';

describe('AutoCompleteDoctorComponent', () => {
  let component: AutoCompleteDoctorComponent;
  let fixture: ComponentFixture<AutoCompleteDoctorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutoCompleteDoctorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoCompleteDoctorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
