import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { PatientService } from "../../patients/allpatients/patient.service";
import { map, startWith } from "rxjs/operators";
import { Patient } from "../../patients/allpatients/patient.model";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { PaymentService } from "../../payment/allpayment/payment.service";
import { HttpClient } from "@angular/common/http";
import { MatSnackBar } from "@angular/material/snack-bar";
import { AdminSettingsService } from "../../../services/AdminSettings.service";
import { DataModelSelect } from "../../../shared/DataModelSelect";
import { Observable } from "rxjs";
import { Payment } from "../../payment/allpayment/payment.model";

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'DevCode-auto-complete-patient',
  templateUrl: './auto-complete-patient.component.html',
  styleUrls: ['./auto-complete-patient.component.sass']
})
export class AutoCompletePatientComponent implements OnInit {
  private dataModel: DataModelSelect = {
    limit: 10,
    offset: 0,
    count: 0,
    pageIndex: 0,
    opcion: "S",
    search: '',
  };
  private _patientService: PatientService | null;
  private DataGetPatient: Patient[] = [];
  public filteredOptions: Observable<Patient[]>;
  public paymentForm: FormGroup;
  private payment: Payment;
  @Output() pk_patient = new EventEmitter<Patient | null>();
  constructor(
    public paymentService: PaymentService,
    private fb: FormBuilder,
    public httpClient: HttpClient,
    public patientService: PatientService,
    private snackBar: MatSnackBar,
    private _AdminSettingsService: AdminSettingsService,
  ) {
    this.payment = new Payment({});
    this.paymentForm = this.createContactForm();
  }
  ngOnInit(): void {
    this.loadData();
  }
  private createContactForm(): FormGroup {
    return this.fb.group({
      PatientName: ['', Validators.required],
    });
  }
  private _filter(value: string): Patient[] {
    const filterValue = value.toLowerCase();
    return this.DataGetPatient?.filter(option => option.paciente_apellidos.toLowerCase().includes(filterValue) || option.paciente_nombres.toLowerCase().includes(filterValue) || option.paciente_cedula.toLowerCase().includes(filterValue));
  }
  public loadData() {
    this._patientService = new PatientService(
      this.httpClient,
      this._AdminSettingsService
    );
    this.GetDataPatiens();
  }
  GetDataPatiens(): void {
    this._patientService
      .crudPaciente(this.dataModel, this.dataModel.opcion)
      .subscribe((resp) => {
        this.DataGetPatient = resp.data.data;
        this.dataModel.count = Math.ceil(resp.data.conteo / this.dataModel.limit);
        this.filteredOptions = this.paymentForm.get('PatientName').valueChanges.pipe(
          startWith(''),
          map(value => this._filter(value)),
        );
      });
  }

  ChangePatient(data: Patient) {
    this.pk_patient.emit(data);
  }

  ChangeStatus() {
    this.pk_patient.emit(null);
  }
}
