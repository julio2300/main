import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoCompletePatientComponent } from './auto-complete-patient.component';

describe('AutoCompletePatientComponent', () => {
  let component: AutoCompletePatientComponent;
  let fixture: ComponentFixture<AutoCompletePatientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutoCompletePatientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoCompletePatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
