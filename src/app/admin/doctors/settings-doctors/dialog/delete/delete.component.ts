import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { Component, Inject } from "@angular/core";
import { DoctorsSettingsService } from "../../setting.service";
import {SpecialtiesModel} from "../../specialties.model";

@Component({
  selector: "app-delete",
  templateUrl: "./delete.component.html",
  styleUrls: ["./delete.component.sass"],
})
export class SpecialtiesDeleteComponent {
  loading: boolean = false;
  constructor(
    public dialogRef: MatDialogRef<SpecialtiesDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SpecialtiesModel[],
    public _SpecialtiesService: DoctorsSettingsService
  ) {}
  onNoClick(): void {
    this.dialogRef.close();
  }
  confirmDelete(): void {
    // this._SpecialtiesService.deletePatient(this.data.pk_doctor);
    this.loading = true;
    this._SpecialtiesService
      .CrudSpecialties({ detalle: this.data }, "D")
      .subscribe((resp) => {
        this.loading = false;
        this.dialogRef.close(1);
      });
  }
}
