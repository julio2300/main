export class SpecialtiesModel {
  pk_espe:number;
  espe_name:string;
  espe_fecha:Date;
  constructor(especialidad) {
    this.pk_espe=especialidad.pk_espe || 0;
    this.espe_name=especialidad.espe_name||'';
    this.espe_fecha=especialidad.fecha||new Date().toISOString();
  }
}
