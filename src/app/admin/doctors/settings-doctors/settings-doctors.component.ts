import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {SpecialtiesModel} from 'src/app/admin/doctors/settings-doctors/specialties.model';
import {FormBuilder, FormGroup,} from "@angular/forms";
import {DataSource, SelectionModel} from "@angular/cdk/collections";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {debounceTime, fromEvent, merge, Observable} from "rxjs";
import {map} from "rxjs/operators";
import {DataModelSelect} from 'src/app/shared/DataModelSelect';
import {DoctorsSettingsService} from "./setting.service";
import {HttpClient} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {AdminSettingsService} from "../../../services/AdminSettings.service";
import {UnsubscribeOnDestroyAdapter} from "../../../shared/UnsubscribeOnDestroyAdapter";
import {SpecialtiesDeleteComponent} from "./dialog/delete/delete.component";

@Component({
  selector: 'app-settings-doctors',
  templateUrl: './settings-doctors.component.html',
  styleUrls: ['./settings-doctors.component.sass']
})
export class SettingsDoctorsComponent extends UnsubscribeOnDestroyAdapter implements OnInit, AfterViewInit {
  especialidad: SpecialtiesModel | null;
  doctorsEspe: FormGroup;
  loading: boolean = false;
  //Variables for  DataTable
  dataSource: DataSourcespecialties | null;
  displayedColumns = [
    "select",
    "img",
    "espe_name",
    "espe_fecha",
    "actions",
  ];
  selection = new SelectionModel<SpecialtiesModel>(true, []);
  source: any;
  SearchDataServe: string;
  dataModel: DataModelSelect = {
    limit: 10,
    offset: 0,
    count: 0,
    pageIndex: 0,
    opcion: "C",
    search: null,
  };
  _SpecialtiesService: DoctorsSettingsService | null;
  title: string = 'Crear';
  btnLoadingDelete: boolean = false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild("filter", {static: true}) filter: ElementRef;
  id: number;
  index: number;

  //End Variables DataTable
  constructor(
    private fb: FormBuilder,
    public httpClient: HttpClient,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private _AdminSettingsService: AdminSettingsService,
    private SpecialtiesService: DoctorsSettingsService
  ) {
    super();
    this.especialidad = new SpecialtiesModel({});
  }

  ngOnInit(): void {
    this.doctorsEspe = this.createDataForm();
    this.loadData();
  }

  createDataForm(): FormGroup {
    return this.fb.group({
      pk_espe: [this.especialidad.pk_espe],
      espe_name: [this.especialidad.espe_name],
      espe_fecha: [this.especialidad.espe_fecha]
    });
  }

  submit() {
    if (this.title == 'Crear') {
      this.SendDataServerInsert();
    } else {
      this.SendDataServerUpdate();
    }
  }

  SendDataServerInsert() {
    this.loading = true;
    let datos = this.doctorsEspe.getRawValue();
    console.log(datos);
    this._SpecialtiesService.CrudSpecialties({detalle: [datos]}, 'I').subscribe(resp => {
      this.loading = false;
      this.showNotification(
        "snackbar-success",
        "Especialidad agregada con éxito...!!!",
        "bottom",
        "center"
      );
      this.refresh();
      this.doctorsEspe.reset();
      this.addNew();
    });
  }

  SendDataServerUpdate() {
    this.loading = true;
    this._SpecialtiesService.CrudSpecialties(this.doctorsEspe.getRawValue(), 'U').subscribe(resp => {
      console.log(resp);
      this.loading = false;
      this.showNotification(
        "snackbar-success",
        "Especialidad editada con éxito...!!!",
        "bottom",
        "center"
      );
      this.refresh();
    });
  }

  //Functions for DataTable
  ngAfterViewInit(): void {
    this.source = fromEvent(this.filter.nativeElement, "keyup");
    this.source.pipe(debounceTime(500)).subscribe((c) => {
      this.dataModel.opcion = "S";
      !this.SearchDataServe?.length && (this.dataModel.opcion = "C");
      this.dataModel.search = this.SearchDataServe;
      this.dataModel.limit = 10;
      this.dataModel.offset = 0;
      this.dataModel.count = 0;
      this.dataModel.pageIndex = 0;
      this.loadData();
    });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.renderedData.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.renderedData.forEach((row) =>
        this.selection.select(row)
      );
  }

  public loadData() {
    this._SpecialtiesService = new DoctorsSettingsService(
      this.httpClient,
      this._AdminSettingsService
    );
    this.newDataRenove();
  }

  newDataRenove() {
    this.dataSource = new DataSourcespecialties(
      this._SpecialtiesService,
      this.paginator,
      this.sort,
      this.dataModel
    );

  }

  getData(page) {
    let {length, pageIndex, pageSize, previousPageIndex} = page;
    this.dataModel.limit = pageSize;
    this.dataModel.pageIndex = pageIndex;
    this.dataModel.offset = pageIndex * this.dataModel.limit;
    this.loadData();
  }

  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, "", {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

  removeSelectedRows() {
    this.btnLoadingDelete = true;
    this.doctorsEspe.reset();
    let rowSelected = this.selection.selected.map(_ => _.pk_espe);
    let data = this._SpecialtiesService.dataChange.value;
    data = data.filter(_ => rowSelected.some(row => row === _.pk_espe));
    const totalSelect = this.selection.selected.length;
    console.log(data);
    this.SpecialtiesService
      .CrudSpecialties({detalle: data}, "D")
      .subscribe((resp) => {
        this.btnLoadingDelete = false;
        this.refresh();
        this.selection = new SelectionModel<SpecialtiesModel>(true, []);
        this.showNotification(
          "snackbar-danger",
          totalSelect + " registros fueron eliminado con éxito...!!!",
          "bottom",
          "center"
        );
      });
  }

  private refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }

  refresh() {
    this.loadData();
  }

  addNew() {
    this.title = 'Crear';
    this.especialidad.pk_espe = null;
    this.especialidad.espe_name = null;
    this.especialidad.espe_fecha = new Date();
    this.doctorsEspe = this.createDataForm();
  }

  editCall(row: SpecialtiesModel) {
    this.title = 'Modificar';
    this.especialidad.pk_espe = row.pk_espe;
    this.especialidad.espe_name = row.espe_name;
    console.log(this.especialidad)
    this.doctorsEspe = this.createDataForm();
  }

  deleteItem(i: number, row: SpecialtiesModel) {
    this.doctorsEspe.reset();
    this.index = i;
    this.id = row.pk_espe;
    let tempDirection;
    if (localStorage.getItem("isRtl") === "true") {
      tempDirection = "rtl";
    } else {
      tempDirection = "ltr";
    }
    const dialogRef = this.dialog.open(SpecialtiesDeleteComponent, {
      data: [row],
      direction: tempDirection,
    });
    this.subs.sink = dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        const foundIndex = this._SpecialtiesService.dataChange.value.findIndex(
          (x) => x.pk_espe === this.id
        );
        // for delete we use splice in order to remove single object from DataService
        this._SpecialtiesService.dataChange.value.splice(foundIndex, 1);
        this.refreshTable();
        this.showNotification(
          "snackbar-danger",
          "Registro eliminado con éxito...!!!",
          "bottom",
          "center"
        );
      }
    });
  }

  //End Functions DataTable
}

//Data Server get for Especialidades
export class DataSourcespecialties extends DataSource<SpecialtiesModel> {
  filteredData: SpecialtiesModel[] = [];
  renderedData: SpecialtiesModel[] = [];

  constructor(
    public _specialtiesService: DoctorsSettingsService,
    public paginator: MatPaginator,
    public _sort: MatSort,
    public dataModel: DataModelSelect
  ) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<SpecialtiesModel[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._specialtiesService.dataChange,
      this._sort.sortChange,
      this.paginator.page,
    ];
    // this._specialtiesService.getAllPatients();
    console.log(this.dataModel);

    this._specialtiesService
      .CrudSpecialties(this.dataModel, this.dataModel.opcion)
      .subscribe((resp) => {
        this.dataModel.count = Math.ceil(
          resp.data.conteo / this.dataModel.limit
        );
      });
    return merge(...displayDataChanges).pipe(
      map(() => {
        // Filter data
        this.filteredData = this._specialtiesService.data;
        // Sort filtered data
        console.log(this._specialtiesService.data);
        const sortedData = this.sortData(this.filteredData.slice());
        return (this.renderedData = sortedData);
      })
    );
  }

  disconnect() {
  }

  /** Returns a sorted copy of the database data. */
  sortData(data: SpecialtiesModel[]): SpecialtiesModel[] {
    if (!this._sort.active || this._sort.direction === "") {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = "";
      let propertyB: number | string = "";
      switch (this._sort.active) {
        case "pk_espe":
          [propertyA, propertyB] = [a.pk_espe, b.pk_espe];
          break;
        case "espe_mame":
          [propertyA, propertyB] = [a.espe_name, b.espe_name];
          break;
        case "espe_fecha":
          [propertyA, propertyB] = [new Date(a.espe_fecha).getTime(), new Date(b.espe_fecha).getTime()];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === "asc" ? 1 : -1)
      );
    });
  }
}


//prueba de sesion en git0002
