import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { Component, Inject } from "@angular/core";
import { PatientService } from "../../doctors.service";
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { Patient } from "../../doctors.model";

@Component({
  selector: "app-form-dialog",
  templateUrl: "./form-dialog.component.html",
  styleUrls: ["./form-dialog.component.sass"],
})
export class FormDialogComponent {
  action: string;
  dialogTitle: string;
  patientForm: FormGroup;
  patient: Patient;
  loading: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<FormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public patientService: PatientService,
    private fb: FormBuilder
  ) {
    // Set the defaults
    this.action = data.action;
    if (this.action === "edit") {
      this.dialogTitle = data.patient.name;
      this.patient = data.patient;
    } else {
      this.dialogTitle = "Nuevo doctor";
      this.patient = new Patient({});
    }
    this.patientForm = this.createContactForm();
  }
  formControl = new FormControl("", [
    Validators.required,
    // Validators.email,
  ]);
  getErrorMessage() {
    return this.formControl.hasError("required")
      ? "Required field"
      : this.formControl.hasError("email")
      ? "Not a valid email"
      : "";
  }
  createContactForm(): FormGroup {
    return this.fb.group({
      pk_doctor: [this.patient.pk_doctor],
      paciente_imagen: [this.patient.paciente_imagen],
      doctor_cedula: [this.patient.doctor_cedula],
      doctor_nombre: [this.patient.doctor_nombre],
      doctor_apellidos: [this.patient.doctor_apellidos],
      doctor_email: [this.patient.doctor_email],
      paciente_direccion: [this.patient.paciente_direccion],
      doctor_telefono: [this.patient.doctor_telefono],
      doctor_registro: [this.patient.doctor_registro],
      doctor_estado_civil: [this.patient.doctor_estado_civil],
      paciente_edad: [this.patient.paciente_edad],
      paciente_genero: [this.patient.paciente_genero],
      doctor_grupo_sanguineo: [this.patient.doctor_grupo_sanguineo],
      // fk_tratamiento: [this.patient.fk_tratamiento],
    });
  }
  submit() {
    // emppty stuff
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  public confirmAdd(): void {
    // this.patientService.addPatient(this.patientForm.getRawValue());
    this.loading = true;
    let opcion = this.action === "edit" ? "U" : "I";
    if(opcion=="I"){
      this.patientService
        .crudPaciente({detalle:[this.patientForm.getRawValue()]}, opcion)
        .subscribe((resp: any) => {
          this.dialogRef.close(1);
          this.loading = false;
        });
        
      }else{
        
        this.patientService
          .crudPaciente(this.patientForm.getRawValue(), opcion)
          .subscribe((resp: any) => {
            this.dialogRef.close(1);
            this.loading = false;
          });
    }
  }
}
