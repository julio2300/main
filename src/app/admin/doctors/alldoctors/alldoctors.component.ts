import {AfterViewInit, Component, ElementRef, OnInit, ViewChild,} from "@angular/core";
import {PatientService} from "./doctors.service";
import {HttpClient} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {Patient} from "./doctors.model";
import {DataModelSelect} from 'src/app/shared/DataModelSelect';
import {DataSource, SelectionModel} from "@angular/cdk/collections";
import {MatSnackBar} from "@angular/material/snack-bar";
import {FormDialogComponent} from "./dialogs/form-dialog/form-dialog.component";
import {DeleteComponent} from "./dialogs/delete/delete.component";
import {debounceTime, fromEvent, merge, Observable,} from "rxjs";
import {map} from "rxjs/operators";
import {UnsubscribeOnDestroyAdapter} from "src/app/shared/UnsubscribeOnDestroyAdapter";
import {AdminSettingsService} from "src/app/services/AdminSettings.service";

@Component({
  selector: "app-alldoctors",
  templateUrl: "./alldoctors.component.html",
  styleUrls: ["./alldoctors.component.sass"],
})
export class AlldoctorsComponent extends UnsubscribeOnDestroyAdapter implements OnInit, AfterViewInit {
  displayedColumns = [
    "select",
    "img",
    "doctor_nombre",
    "doctor_apellidos",
    "doctor_cedula",
    "doctor_telefono",
    "doctor_registro",
    "doctor_estado_civil",
    "doctor_grupo_sanguineo",
    "actions",
  ];
  _patientService: PatientService | null;
  dataSource: DataSourcePatient | null;
  selection = new SelectionModel<Patient>(true, []);
  index: number;
  id: string;
  patient: Patient | null;
  dataModel: DataModelSelect = {
    limit: 10,
    offset: 0,
    count: 0,
    pageIndex: 0,
    opcion: "C",
    search: null,
  };
  source: any;
  SearchDataServe: string;
  btnLoadingDelete: boolean=false;
  constructor(
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public patientService: PatientService,
    private snackBar: MatSnackBar,
    private _AdminSettingsService: AdminSettingsService
  ) {
    super();
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild("filter", {static: true}) filter: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  ngAfterViewInit(): void {
    this.source = fromEvent(this.filter.nativeElement, "keyup");
    this.source.pipe(debounceTime(500)).subscribe((c) => {
      this.dataModel.opcion = "S";
      !this.SearchDataServe?.length && (this.dataModel.opcion = "C");
      this.dataModel.search = this.SearchDataServe;
      this.dataModel.limit = 10;
      this.dataModel.offset = 0;
      this.dataModel.count = 0;
      this.dataModel.pageIndex = 0;
      this.loadData();
    });
  }

  refresh() {
    this.loadData();
  }

  addNew() {
    let tempDirection;
    if (localStorage.getItem("isRtl") === "true") {
      tempDirection = "rtl";
    } else {
      tempDirection = "ltr";
    }
    const dialogRef = this.dialog.open(FormDialogComponent, {
      data: {
        patient: this.patient,
        action: "add",
      },
      width: "1000px",
      direction: tempDirection,
    });
    this.subs.sink = dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        this.refreshTable();
        this.showNotification(
          "snackbar-success",
          "Producto agregado con éxito...!!!",
          "bottom",
          "center"
        );
      }
    });
  }

  editCall(row) {
    this.id = row.pk_doctor;
    let tempDirection;
    if (localStorage.getItem("isRtl") === "true") {
      tempDirection = "rtl";
    } else {
      tempDirection = "ltr";
    }
    const dialogRef = this.dialog.open(FormDialogComponent, {
      data: {
        patient: row,
        action: "edit",
      },
      direction: tempDirection,
    });
    this.subs.sink = dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        // When using an edit things are little different, firstly we find record inside DataService by id
        const foundIndex = this._patientService.dataChange.value.findIndex(
          (x) => x.pk_doctor === this.id
        );
        // Then you update that record using data from dialogData (values you enetered)
        let data = (this._patientService.dataChange.value[foundIndex] =
          this.patientService.getDialogData());
        console.log(data);

        // And lastly refresh table
        this.refreshTable();
        this.showNotification(
          "black",
          "Registro actualizado con éxito..!!!",
          "bottom",
          "center"
        );
      }
    });
  }

  deleteItem(i: number, row) {
    this.index = i;
    this.id = row.pk_doctor;
    let tempDirection;
    if (localStorage.getItem("isRtl") === "true") {
      tempDirection = "rtl";
    } else {
      tempDirection = "ltr";
    }
    const dialogRef = this.dialog.open(DeleteComponent, {
      data: row,
      direction: tempDirection,
    });
    this.subs.sink = dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        const foundIndex = this._patientService.dataChange.value.findIndex(
          (x) => x.pk_doctor === this.id
        );
        // for delete we use splice in order to remove single object from DataService
        this._patientService.dataChange.value.splice(foundIndex, 1);
        this.refreshTable();
        this.showNotification(
          "snackbar-danger",
          "Registro eliminado con éxito...!!!",
          "bottom",
          "center"
        );
      }
    });
  }

  private refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.renderedData.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.renderedData.forEach((row) =>
        this.selection.select(row)
      );
  }

  removeSelectedRows() {
    this.btnLoadingDelete=true;
    let rowSelected=this.selection.selected.map(_=>_.pk_doctor);
    let data = this._patientService.dataChange.value;
    data=data.filter(_=>rowSelected.some(row=>row===_.pk_doctor));
    const totalSelect = this.selection.selected.length;
    console.log(data);
      this.patientService
        .crudPaciente({detalle: data}, "D")
        .subscribe((resp) => {
          this.btnLoadingDelete=false;
          this.refreshTable();
          this.selection = new SelectionModel<Patient>(true, []);
        });
      this.showNotification(
        "snackbar-danger",
        totalSelect + " Registros eliminado con éxito...!!!",
        "bottom",
        "center"
      );
  }

  public loadData() {
    this._patientService = new PatientService(
      this.httpClient,
      this._AdminSettingsService
    );
    this.dataSource = new DataSourcePatient(
      this._patientService,
      this.paginator,
      this.sort,
      this.dataModel
    );
  }

  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, "", {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

  getData(page) {
    let {length, pageIndex, pageSize, previousPageIndex} = page;
    this.dataModel.limit = pageSize;
    this.dataModel.pageIndex = pageIndex;
    this.dataModel.offset = pageIndex * this.dataModel.limit;
    this.loadData();
  }
}

export class DataSourcePatient extends DataSource<Patient> {
  filteredData: Patient[] = [];
  renderedData: Patient[] = [];

  constructor(
    public _patientService: PatientService,
    public paginator: MatPaginator,
    public _sort: MatSort,
    public dataModel: DataModelSelect
  ) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Patient[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._patientService.dataChange,
      this._sort.sortChange,
      this.paginator.page,
    ];
    // this._patientService.getAllPatients();
    console.log(this.dataModel);

    this._patientService
      .crudPaciente(this.dataModel, this.dataModel.opcion)
      .subscribe((resp) => {
        this.dataModel.count = Math.ceil(
          resp.data.conteo / this.dataModel.limit
        );
      });
    return merge(...displayDataChanges).pipe(
      map(() => {
        // Filter data
        this.filteredData = this._patientService.data;
        // Sort filtered data
        console.log(this._patientService.data);
        const sortedData = this.sortData(this.filteredData.slice());
        return (this.renderedData = sortedData);
      })
    );
  }

  disconnect() {
  }

  /** Returns a sorted copy of the database data. */
  sortData(data: Patient[]): Patient[] {
    if (!this._sort.active || this._sort.direction === "") {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = "";
      let propertyB: number | string = "";
      switch (this._sort.active) {
        case "id":
          [propertyA, propertyB] = [a.pk_doctor, b.pk_doctor];
          break;
        case "name":
          [propertyA, propertyB] = [a.doctor_apellidos, b.doctor_apellidos];
          break;
        case "gender":
          [propertyA, propertyB] = [a.doctor_nombre, b.doctor_nombre];
          break;
        case "date":
          [propertyA, propertyB] = [a.doctor_registro, b.doctor_registro];
          break;
        case "address":
          [propertyA, propertyB] = [a.paciente_direccion, b.paciente_direccion];
          break;
        case "mobile":
          [propertyA, propertyB] = [a.doctor_telefono, b.doctor_telefono];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === "asc" ? 1 : -1)
      );
    });
  }
}
