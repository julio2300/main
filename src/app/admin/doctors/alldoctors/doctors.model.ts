export class Patient {
  pk_doctor: string | null;
  paciente_imagen: string;
  doctor_cedula: string;
  doctor_nombre: string;
  doctor_apellidos: string;
  doctor_email: string;
  paciente_direccion: string;
  doctor_telefono: string;
  doctor_registro: string;
  paciente_edad: string;
  doctor_grupo_sanguineo: string;
  paciente_genero: string;
  doctor_estado_civil: number;
  constructor(patient) {
    {
      this.pk_doctor = patient.pk_doctor || "";
      this.paciente_imagen =
        patient.paciente_imagen || "assets/images/user/user1.jpg";
      this.doctor_cedula = patient.doctor_cedula || "";
      this.doctor_nombre = patient.doctor_nombre || "";
      this.doctor_apellidos = patient.doctor_apellidos || "";
      this.doctor_email = patient.doctor_email || "";
      this.paciente_direccion = patient.paciente_direccion || "";
      this.doctor_telefono = patient.doctor_telefono || "";
      this.doctor_registro = patient.doctor_registro || "";
      this.doctor_registro = patient.doctor_registro || "";
      this.paciente_edad = patient.paciente_edad || "";
      this.doctor_grupo_sanguineo = patient.doctor_grupo_sanguineo || "";
      this.paciente_genero = patient.paciente_genero || "";
      this.doctor_grupo_sanguineo = patient.doctor_grupo_sanguineo || 0;
      this.doctor_estado_civil = patient.doctor_estado_civil || 0;
    }
  }
  public getRandomID(): string {
    var S4 = function () {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return S4() + S4();
  }
}
export class DataModelPaginator {
  length: number;
  pageIndex: number;
  pageSize: number;
  previousPageIndex: number;
}
