import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Patient } from "./doctors.model";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";
import { UnsubscribeOnDestroyAdapter } from "src/app/shared/UnsubscribeOnDestroyAdapter";
import { AdminSettingsService } from "src/app/services/AdminSettings.service";
import { environment } from "src/environments/environment";

import { dominio_ws } from "src/app/config/SettingsGlobal";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { catchError } from "rxjs/operators";
import { throwError } from "rxjs";

@Injectable()
export class PatientService extends UnsubscribeOnDestroyAdapter {
  private readonly API_URL = "assets/data/patient.json";

  isTblLoading = true;
  dataChange: BehaviorSubject<Patient[]> = new BehaviorSubject<Patient[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  actions: any;
  constructor(
    private httpClient: HttpClient,
    private _AdminSettingsService: AdminSettingsService
  ) {
    super();
    this.actions = {
      C: (data) => {
        this.dataChange.next(data);
      },
      U: (patient) => {
        this.dialogData = patient;
      },
      D: (data) => {},
      I: (data) => {
        console.log(this.data);
        this.dataChange.next(data);
      },
      S: (data) => {
        this.dataChange.next(data);
      },
    };
  }
  get data(): Patient[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }
  /** CRUD METHODS */
  getAllPatients(): void {
    this.subs.sink = this.httpClient.get<Patient[]>(this.API_URL).subscribe(
      (data) => {
        this.isTblLoading = false;
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        this.isTblLoading = false;
        console.log(error.name + " " + error.message);
      }
    );
  }
  addPatient(patient: Patient): void {
    this.dialogData = patient;

    /*  this.httpClient.post(this.API_URL, patient).subscribe(data => {
      this.dialogData = patient;
      },
      (err: HttpErrorResponse) => {
     // error code here
    });*/
  }

  updatePatient(patient: Patient): void {
    this.dialogData = patient;

    /* this.httpClient.put(this.API_URL + patient.id, patient).subscribe(data => {
      this.dialogData = patient;
    },
    (err: HttpErrorResponse) => {
      // error code here
    }
  );*/
  }
  deletePatient(id: number): void {
    console.log(id);

    /*  this.httpClient.delete(this.API_URL + id).subscribe(data => {
      console.log(id);
      },
      (err: HttpErrorResponse) => {
         // error code here
      }
    );*/
  }

  crudPaciente(Data, opcion) {
    this._AdminSettingsService.cargarValoresLocalStorage();
    let tkJwt = this._AdminSettingsService.MyData.token;

    let headers = new HttpHeaders();
    headers = headers.set("token", tkJwt);

    let QueryData = {
      data: this._AdminSettingsService.encryptData({
        opcion: opcion,
        pk_empresa: 1,
        json: Data,
      }),
    };

    return this.httpClient
      .post(environment.apiServer+environment.apiserverRutas.doctors, QueryData, { headers })
      .pipe(
        map((resp: any) => {
          let data = this._AdminSettingsService.decryptData(resp.data);
          console.log(data);
          this.actions[opcion](data.data.data || []);
          this.isTblLoading = false;
          return data;
        })
      )
      .pipe(
        catchError((err) => {
          console.log("Error no controlado " + JSON.stringify(err));
          return throwError(err);
        })
      );
  }
}
