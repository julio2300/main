import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AdminRoutingModule } from "./admin-routing.module";
import {MatIconModule} from "@angular/material/icon";
import {MatFormFieldModule} from "@angular/material/form-field";
import { SharedModule } from "./shared/shared.module";
@NgModule({
  declarations: [
  ],
  imports: [CommonModule, AdminRoutingModule, MatIconModule, MatFormFieldModule],
  exports:[SharedModule]
})
export class AdminModule {}
