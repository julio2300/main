import { Injectable } from "@angular/core";
import { BehaviorSubject, catchError, map, throwError } from "rxjs";
import { Payment } from "./payment.model";
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { UnsubscribeOnDestroyAdapter } from "src/app/shared/UnsubscribeOnDestroyAdapter";
import { AdminSettingsService } from "src/app/services/AdminSettings.service";
import { environment } from "src/environments/environment";
@Injectable()
export class PaymentService extends UnsubscribeOnDestroyAdapter {
  private readonly API_URL = "assets/data/payment.json";
  isTblLoading = true;
  dataChange: BehaviorSubject<Payment[]> = new BehaviorSubject<Payment[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  actions: any;

  constructor(
    private httpClient: HttpClient,
    private _AdminSettingsService: AdminSettingsService
  ) {
    super();
    this.actions = {
      C: (data) => {
        this.dataChange.next(data);
      },
      U: (patient) => {
        this.dialogData = patient;
      },
      D: (data) => {
      },
      I: (data) => {
        this.dataChange.next(data);
      },
      S: (data) => {
        this.dataChange.next(data);
      },
    };
  }
  get data(): Payment[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }
  /** CRUD METHODS */
  getAllPayments(): void {
    this.subs.sink = this.httpClient.get<Payment[]>(this.API_URL).subscribe(
      (data) => {
        this.isTblLoading = false;
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        this.isTblLoading = false;
        console.log(error.name + " " + error.message);
      }
    );
  }
  addPayment(payment: Payment): void {
    this.dialogData = payment;

    /*  this.httpClient.post(this.API_URL, payment).subscribe(data => {
      this.dialogData = payment;
      },
      (err: HttpErrorResponse) => {
     // error code here
    });*/
  }
  updatePayment(payment: Payment): void {
    this.dialogData = payment;

    /* this.httpClient.put(this.API_URL + payment.id, payment).subscribe(data => {
      this.dialogData = payment;
    },
    (err: HttpErrorResponse) => {
      // error code here
    }
  );*/
  }
  deletePayment(id: number): void {
    console.log(id);

    /*  this.httpClient.delete(this.API_URL + id).subscribe(data => {
      console.log(id);
      },
      (err: HttpErrorResponse) => {
         // error code here
      }
    );*/
  }

  CrudInvoice(Data, opcion) {
    this._AdminSettingsService.cargarValoresLocalStorage();
    let tkJwt = this._AdminSettingsService.MyData.token;
    let user = this._AdminSettingsService.getDecodedAccessToken(tkJwt).user[0].datos;
    Data.pk_user = user.id;

    let headers = new HttpHeaders();
    headers = headers.set("token", tkJwt);

    let QueryData = {
      data: this._AdminSettingsService.encryptData({
        opcion: opcion,
        pk_empresa: 1,
        json: Data,
      }),
    };
    return this.httpClient
      .post(environment.apiServer + environment.apiserverRutas.invoice, QueryData, { headers })
      .pipe(
        map((resp: any) => {
          let data = this._AdminSettingsService.decryptData(resp.data);
          // console.log(data);
          this.actions[opcion](data.data.data || []);
          this.isTblLoading = false;
          return data;
        })
      )
      .pipe(
        catchError((err) => {
          console.log("Error no controlado " + JSON.stringify(err));
          return throwError(err);
        })
      );
  }
}
