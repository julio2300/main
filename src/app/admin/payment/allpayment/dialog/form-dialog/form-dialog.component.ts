import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from "@angular/material/dialog";
import { Component, Inject, OnInit, ViewChild } from "@angular/core";
import { PaymentService } from "../../payment.service";
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { Payment } from "../../payment.model";
import { Observable } from "rxjs";
import { map, startWith } from "rxjs/operators";
import { PatientService } from "../../../../patients/allpatients/patient.service";
import { DataSourcePatient } from "../../../../patients/allpatients/allpatients.component";
import { HttpClient } from "@angular/common/http";
import { MatSnackBar } from "@angular/material/snack-bar";
import { AdminSettingsService } from "../../../../../services/AdminSettings.service";
import { DataModelPaginator, Patient } from "src/app/admin/patients/allpatients/patient.model";
import { DataModelSelect } from "../../../../../shared/DataModelSelect";
import { STEPPER_GLOBAL_OPTIONS } from "@angular/cdk/stepper";
import { Patient as Doctor } from "src/app/admin/doctors/alldoctors/doctors.model";
import { PaymentDetail } from "../../payment-detail.model ";
import { MatAccordion } from '@angular/material/expansion';
import * as moment from 'moment';
@Component({
  selector: "app-form-dialog",
  templateUrl: "./form-dialog.component.html",
  styleUrls: ["./form-dialog.component.scss"],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { showError: true },
    },
  ],
})
export class FormDialogComponent implements OnInit {
  private action: string;
  public dialogTitle: string;
  public FirstPaymentForm: FormGroup;
  public SecondPaymentForm: FormGroup;
  public payment: Payment;
  public paymentDetail: PaymentDetail;
  public Representante: any;

  public InvoiceDetail: PaymentDetail[] = [{
    // en la posicion cero siempre va a estar reservada para pagos mensuales
    pk_detail: null,
    details_cant: null,
    details_total: null,
    fk_producto: null,
    fk_invoice: null,
    fk_plan: null,
  }
    // ,{
    //   // en la posicion uno siempre va a estar reservada para pagos de productos
    //   pk_detail: null,
    //   details_cant: null,
    //   details_total: null,
    //   fk_producto: null,
    //   fk_invoice: null,
    //   fk_plan: null,
    // }
  ];
  public formControl = new FormControl("", [
    Validators.required,
    // Validators.email,
  ]);

  private dataSource: DataSourcePatient | null;
  public pk_patient: number;
  private pk_doctor: number;
  public CheckRepre: boolean = false;
  public Months: any = [];
  private MonthsReal: any;
  public ShowData: any = {
    repre_cedula: '',
    repre_apellidos: '',
    repre_nombres: '',
    repre_telefono: '',
    repre_direccion: '',
    repre_correo: '',
  };

  private SetPatient: any;
  public LoadingSaveData: boolean = false;

  @ViewChild(MatAccordion) public accordion: MatAccordion;

  constructor(
    public dialogRef: MatDialogRef<FormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public paymentService: PaymentService,
    private fb: FormBuilder,
    public httpClient: HttpClient,
    public patientService: PatientService,
    private snackBar: MatSnackBar,
    private _AdminSettingsService: AdminSettingsService,
  ) {
    // Set the defaults
    this.action = data.action;
    if (this.action === "edit") {
      this.dialogTitle = data.payment.pName;
      this.payment = data.payment;
    } else {
      this.dialogTitle = "Nuevo Pago";
      this.payment = new Payment({});
      this.paymentDetail = new PaymentDetail({});
    }
    this.FirstPaymentForm = this.CreateDataFromFirst();

    this.FirstPaymentForm.get('invoice_desc').valueChanges.subscribe((status) => {
      this.PaymentAvg({ desc: status });
    })
    this.FirstPaymentForm.valueChanges.subscribe((value) => {
      if (this.CheckRepre && value.pk_repre === 0) {
        this.SetterShowData(value);
      }
    });
  }

  ngOnInit() {

  }

  getErrorMessage() {
    return this.formControl.hasError("required")
      ? "Required field"
      : this.formControl.hasError("email")
        ? "Not a valid email"
        : "";
  }

  CreateDataFromFirst(): FormGroup {
    return this.fb.group({
      fk_paciente: [this.payment.fk_paciente, Validators.required],
      fk_doctor: [this.payment.fk_doctor, Validators.required],
      invoice_fecha: [this.payment.invoice_fecha, Validators.required],
      invoice_desc: [this.payment.invoice_desc, Validators.required],
      invoice_repre: [false, Validators.nullValidator],
      pk_repre: [0, Validators.required],
      repre_cedula: ['0', Validators.required],
      repre_apellidos: ['0', Validators.required],
      repre_nombres: ['0', Validators.required],
      repre_telefono: ['0', Validators.required],
      repre_direccion: ['0', Validators.required],
      repre_correo: ['0', Validators.required],
    });
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    delete this.payment.fk_doctor;
    delete this.payment.fk_paciente;
    delete this.payment.fk_repre;
    delete this.payment.fk_user;

    let DataSenServer = {
      detalle: [{
        ...this.FirstPaymentForm.getRawValue(),
        ...this.payment,
        DetalleInvoice: [...this.InvoiceDetail]
      }]
    };
    this.LoadingSaveData = true;
    let opcion = this.action === "edit" ? "U" : "I";
    this.paymentService.CrudInvoice(DataSenServer, opcion).subscribe((resp: any) => {
      this.dialogRef.close(1);
      this.LoadingSaveData = false;
    });
    // this.paymentService.addPayment(this.FirstPaymentForm.getRawValue());
  }

  GetPkPatient(PatientData: Patient | null): void {
    this.pk_patient = +PatientData?.pk_paciente;
    this.FirstPaymentForm.get('fk_paciente').setValue(this.pk_patient || '');
    if (!PatientData) {
      this.MonthsReal = null;
      return;
    }
    PatientData.plan.plan_desc = this.ValDesc(PatientData.plan.plan_fecha) > PatientData.plan.plan_desc_meses ? 0 : PatientData.plan.plan_desc;
    PatientData.plan.plan_total = PatientData.plan.plan_valor - (PatientData.plan.plan_desc * PatientData.plan.plan_valor);
    this.MonthsReal = PatientData.plan;
    this.paymentDetail.fk_plan = PatientData.fk_plan;
    this.Representante = PatientData.representantes;
    this.Representante = this.Representante?.map($$ => {
      $$.repre_indeterminated = false;
      return $$;
    });
    this.SetPatient = {
      repre_cedula: PatientData.paciente_cedula,
      repre_apellidos: PatientData.paciente_apellidos,
      repre_nombres: PatientData.paciente_nombres,
      repre_telefono: PatientData.paciente_celular,
      repre_direccion: PatientData.paciente_direccion,
      repre_correo: PatientData.paciente_correo
    };
    this.SetterShowData(this.SetPatient);
  }

  SetterShowData(Data: any, val: boolean = false) {
    this.ShowData.repre_cedula = val ? '' : Data.repre_cedula;
    this.ShowData.repre_apellidos = val ? '' : Data.repre_apellidos;
    this.ShowData.repre_nombres = val ? '' : Data.repre_nombres;
    this.ShowData.repre_telefono = val ? '' : Data.repre_telefono;
    this.ShowData.repre_direccion = val ? '' : Data.repre_direccion;
    this.ShowData.repre_correo = val ? '' : Data.repre_correo;
  }

  ValDesc(date: string): Number {
    let now = moment(new Date());
    let end = moment(date);
    let duration = moment.duration(now.diff(end));
    return duration.asMonths();
  }

  GetPkDoctor(DoctorData: Doctor | null): void {
    this.pk_doctor = +DoctorData?.pk_doctor;
    this.FirstPaymentForm.get('fk_doctor').setValue(this.pk_doctor || '');
  }

  ShowRepre() {
    this.SetterForm(this.CheckRepre);
    this.CheckRepre = !this.CheckRepre;

    this.Representante = this.Representante?.map($$ => {
      $$.repre_indeterminated = false;
      return $$;
    });

    this.SetterShowData(this.SetPatient, this.CheckRepre);
    this.FirstPaymentForm.get('pk_repre').setValue(0);
  }

  verificacion() {
    console.log(this.FirstPaymentForm.getRawValue());
  }

  SelectedRepre(pk_repre: number) {
    let value = this.FirstPaymentForm.get('pk_repre').value;
    this.FirstPaymentForm.get('pk_repre').setValue(value === 0 ? pk_repre : 0);
    this.SetterForm(value === 0);
    this.Representante = this.Representante?.map($$ => {
      $$.repre_indeterminated = !$$.repre_indeterminated;
      $$.pk_repre == pk_repre && ($$.repre_indeterminated = !$$.repre_indeterminated);
      return $$;
    });

    let repre = this.Representante.find($$ => $$.pk_repre == pk_repre);
    value = this.FirstPaymentForm.get('pk_repre').value;
    this.SetterShowData(repre, value === 0);
  }

  SetterForm(CheckRepre: boolean) {
    this.FirstPaymentForm.get('repre_cedula').setValue(CheckRepre ? '0' : null);
    this.FirstPaymentForm.get('repre_apellidos').setValue(CheckRepre ? '0' : null);
    this.FirstPaymentForm.get('repre_nombres').setValue(CheckRepre ? '0' : null);
    this.FirstPaymentForm.get('repre_telefono').setValue(CheckRepre ? '0' : null);
    this.FirstPaymentForm.get('repre_direccion').setValue(CheckRepre ? '0' : null);
    this.FirstPaymentForm.get('repre_correo').setValue(CheckRepre ? '0' : null);
  }

  AddMonth() {
    if (!this.MonthsReal) {
      this.showNotification(
        "snackbar-danger",
        "Paciente no seleccionado",
        "bottom",
        "center"
      );
    } else {
      this.Months.push(this.MonthsReal);
      this.InvoiceDetail[0].fk_plan = this.MonthsReal.pk_plan;
      this.InvoiceDetail[0].details_cant = this.Months.length;
      this.InvoiceDetail[0].details_total = this.Months.reduce((acu, crr) => ({ plan_total: acu?.plan_total + crr?.plan_total })).plan_total;
      this.PaymentAvg({ subTotal0: this.InvoiceDetail[0].details_total });
    }
  }

  RemoveMonth(indice: number) {
    this.InvoiceDetail[0].details_cant--;
    this.InvoiceDetail[0].details_total -= this.Months[indice].plan_total;
    this.PaymentAvg({ subTotal0: this.Months[indice].plan_total });
    this.Months.splice(indice, 1);
  }

  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, "", {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

  PaymentAvg({ subTotal0 = 0, subTotal12 = 0, desc = 0 }) {
    this.payment.invoice_subTotal0 += subTotal0;
    this.payment.invoice_subTotal12 += subTotal12;
    this.payment.invoice_subTotal += subTotal0 + subTotal12;
    this.payment.invoice_desc = desc;
    this.payment.invoice_total = this.payment.invoice_subTotal - (this.payment.invoice_subTotal * (this.payment.invoice_desc / 100));
  }
}
