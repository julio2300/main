export class Payment {
  pk_invoice: number;
  invoice_fecha: Date;
  invoice_subTotal0: number;
  invoice_subTotal12: number;
  invoice_subTotal: number;
  invoice_total: number;
  invoice_secuencial: string;
  invoice_desc: number;
  fk_doctor: number;
  fk_paciente: number;
  fk_user: number;
  fk_repre: number;

  constructor(payment) {
    {
      this.pk_invoice = payment.pk_invoice || this.getRandomID();
      this.invoice_fecha = payment.invoice_fecha || new Date().toISOString();
      this.invoice_subTotal0 = payment.invoice_subTotal0 || 0;
      this.invoice_subTotal12 = payment.invoice_subTotal12 || 0;
      this.invoice_subTotal = payment.invoice_subTotal || 0;
      this.invoice_total = payment.invoice_total || 0;
      this.invoice_secuencial = payment.invoice_secuencial || "";
      this.invoice_desc = payment.invoice_desc || 0;
      this.fk_doctor = payment.fk_doctor || "";
      this.fk_paciente = payment.fk_paciente || "";
      this.fk_user = payment.fk_user || "";
      this.fk_repre = payment.fk_repre || "";
    }
  }
  public getRandomID(): string {
    const S4 = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return S4() + S4();
  }
}
