import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { PaymentService } from "./payment.service";
import { HttpClient } from "@angular/common/http";
import { MatDialog } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { Payment } from "./payment.model";
import { DataSource } from "@angular/cdk/collections";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormDialogComponent } from "./dialog/form-dialog/form-dialog.component";
import { DeleteDialogComponent } from "./dialog/delete/delete.component";
import { BehaviorSubject, fromEvent, merge, Observable, debounceTime } from "rxjs";
import { map } from "rxjs/operators";
import { SelectionModel } from "@angular/cdk/collections";
import { UnsubscribeOnDestroyAdapter } from "src/app/shared/UnsubscribeOnDestroyAdapter";
import { AdminSettingsService } from "src/app/services/AdminSettings.service";
import { DataModelSelect } from "src/app/shared/DataModelSelect";

@Component({
  selector: "app-allpayment",
  templateUrl: "./allpayment.component.html",
  styleUrls: ["./allpayment.component.sass"],
})
export class AllpaymentComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  displayedColumns = [
    "select",
    "patient",
    "doctor",
    "date",
    "subt",
    "disc",
    "total",
    "actions",
  ];

  dataSource: DataSourcePayment | null;
  selection = new SelectionModel<Payment>(true, []);
  index: number;
  id: number;
  payment: Payment | null;

  dataModel: DataModelSelect = {
    limit: 10,
    offset: 0,
    count: 0,
    pageIndex: 0,
    opcion: "C",
    search: '',
  };

  btnLoadingDelete: boolean = false;
  source: any;
  SearchDataServe: string;

  constructor(
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public paymentService: PaymentService,
    private snackBar: MatSnackBar,
    private _AdminSettingsService: AdminSettingsService,

  ) {
    super();
  }
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild("filter", { static: true }) filter: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  ngAfterViewInit(): void {
    this.source = fromEvent(this.filter.nativeElement, "keyup");
    this.source.pipe(debounceTime(500)).subscribe((c) => {
      // this.dataModel.opcion = "C";
      // !this.SearchDataServe?.length && (this.dataModel.opcion = "C");
      this.dataModel.search = this.SearchDataServe;
      this.dataModel.limit = 10;
      this.dataModel.offset = 0;
      this.dataModel.count = 0;
      this.dataModel.pageIndex = 0;
      this.loadData();
    });
  }

  refresh() {
    this.loadData();
  }
  addNew() {
    let tempDirection;
    if (localStorage.getItem("isRtl") === "true") {
      tempDirection = "rtl";
    } else {
      tempDirection = "ltr";
    }
    const dialogRef = this.dialog.open(FormDialogComponent, {
      data: {
        payment: this.payment,
        action: "add",
      },
      direction: tempDirection,
      width: '900px'
    });
    this.subs.sink = dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        this.refreshTable();
        this.showNotification(
          "snackbar-success",
          "Pago añadido con éxito...!!!",
          "bottom",
          "center"
        );
      }
    });
  }
  editCall(row) {
    this.id = row.id;
    let tempDirection;
    if (localStorage.getItem("isRtl") === "true") {
      tempDirection = "rtl";
    } else {
      tempDirection = "ltr";
    }
    const dialogRef = this.dialog.open(FormDialogComponent, {
      data: {
        payment: row,
        action: "edit",
      },
      direction: tempDirection,
    });
    this.subs.sink = dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        // When using an edit things are little different, firstly we find record inside DataService by id
        const foundIndex = this.paymentService.dataChange.value.findIndex(
          (x) => x.pk_invoice === this.id
        );
        // Then you update that record using data from dialogData (values you enetered)
        this.paymentService.dataChange.value[foundIndex] =
          this.paymentService.getDialogData();
        // And lastly refresh table
        this.refreshTable();
        this.showNotification(
          "black",
          "Edit Record Successfully...!!!",
          "bottom",
          "center"
        );
      }
    });
  }
  deleteItem(i: number, row) {
    this.index = i;
    this.id = row.id;
    let tempDirection;
    if (localStorage.getItem("isRtl") === "true") {
      tempDirection = "rtl";
    } else {
      tempDirection = "ltr";
    }
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: row,
      direction: tempDirection,
    });
    this.subs.sink = dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        const foundIndex = this.paymentService.dataChange.value.findIndex(
          (x) => x.pk_invoice === this.id
        );
        // for delete we use splice in order to remove single object from DataService
        this.paymentService.dataChange.value.splice(foundIndex, 1);
        this.refreshTable();
        this.showNotification(
          "snackbar-danger",
          "Delete Record Successfully...!!!",
          "bottom",
          "center"
        );
      }
    });
  }
  private refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.renderedData.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.renderedData.forEach((row) =>
        this.selection.select(row)
      );
  }
  removeSelectedRows() {
    this.btnLoadingDelete = true;
    let rowSelected = this.selection.selected.map(_ => _.pk_invoice);
    let data = this.paymentService.dataChange.value;
    data = data.filter(_ => rowSelected.some(row => row === _.pk_invoice));
    const totalSelect = this.selection.selected.length;
    this.paymentService
      .CrudInvoice({ detalle: data }, "D")
      .subscribe((resp) => {
        this.btnLoadingDelete = false;
        this.refreshTable();
        this.selection = new SelectionModel<Payment>(true, []);
      });
    this.showNotification(
      "snackbar-danger",
      totalSelect + " Registros eliminado con éxito...!!!",
      "bottom",
      "center"
    );
  }
  public loadData() {
    this.paymentService = new PaymentService(
      this.httpClient,
      this._AdminSettingsService
    );
    this.dataSource = new DataSourcePayment(
      this.paginator,
      this.sort,
      this.paymentService,
      this.dataModel
    );
  }
  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, "", {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

  getData(page) {
    let { length, pageIndex, pageSize, previousPageIndex } = page;
    this.dataModel.limit = pageSize;
    this.dataModel.pageIndex = pageIndex;
    this.dataModel.offset = pageIndex * this.dataModel.limit;
    this.loadData();
  }
}
export class DataSourcePayment extends DataSource<Payment> {
  filterChange = new BehaviorSubject("");
  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: Payment[] = [];
  renderedData: Payment[] = [];
  constructor(
    public paginator: MatPaginator,
    public _sort: MatSort,
    public paymentService: PaymentService,
    public dataModel: DataModelSelect

  ) {
    super();
    // Reset to the first page when the user changes the filter.
    this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));
  }
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Payment[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this.paymentService.dataChange,
      this._sort.sortChange,
      this.filterChange,
      this.paginator.page,
    ];
    // this.paymentService.getAllPayments();
    this.paymentService.CrudInvoice(this.dataModel, this.dataModel.opcion).subscribe((resp) => {
      this.dataModel.count = resp.data.conteo;
    });
    return merge(...displayDataChanges).pipe(
      map(() => {
        // Filter data
        this.filteredData = this.paymentService.data;
        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());
        // Grab the page's slice of the filtered sorted data.
        return (this.renderedData = sortedData);
      })
    );
  }
  disconnect() { }
  /** Returns a sorted copy of the database data. */
  sortData(data: Payment[]): Payment[] {
    if (!this._sort.active || this._sort.direction === "") {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string | Date = "";
      let propertyB: number | string | Date = "";
      switch (this._sort.active) {
        case "id":
          [propertyA, propertyB] = [a.pk_invoice, b.pk_invoice];
          break;
        case "pName":
          [propertyA, propertyB] = [a.invoice_fecha, b.invoice_fecha];
          break;
        case "dName":
          [propertyA, propertyB] = [a.invoice_secuencial, b.invoice_secuencial];
          break;
        case "charges":
          [propertyA, propertyB] = [a.invoice_total, b.invoice_total];
          break;
        case "date":
          [propertyA, propertyB] = [a.invoice_subTotal, b.invoice_subTotal];
          break;
        case "discount":
          [propertyA, propertyB] = [a.invoice_desc, b.invoice_desc];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === "asc" ? 1 : -1)
      );
    });
  }
}
