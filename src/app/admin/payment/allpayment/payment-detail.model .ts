export class PaymentDetail {
  pk_detail: number | null;
  details_cant: number | null;
  details_total: number | null;
  fk_producto: number | null;
  fk_invoice: number | null;
  fk_plan: number | null;
  constructor(detail) {
    {
      this.pk_detail = detail.pk_detail || null;
      this.details_cant = detail.details_cant || null;
      this.details_total = detail.details_total || null;
      this.fk_producto = detail.fk_producto || null;
      this.fk_invoice = detail.fk_invoice || null;
      this.fk_plan = detail.fk_plan || null;
    }
  }
}
