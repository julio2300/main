import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatSelectModule } from "@angular/material/select";
import { MatRadioModule } from "@angular/material/radio";
import { MatDialogModule } from "@angular/material/dialog";
import { MatMenuModule } from "@angular/material/menu";
import { MatSortModule } from "@angular/material/sort";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MaterialFileInputModule } from "ngx-material-file-input";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { PatientsRoutingModule } from "./patients-routing.module";
import { AddPatientComponent } from "./add-patient/add-patient.component";
import { AllpatientsComponent } from "./allpatients/allpatients.component";
import { EditPatientComponent } from "./edit-patient/edit-patient.component";
import { PatientProfileComponent } from "./patient-profile/patient-profile.component";
import { DeleteComponent } from "./allpatients/dialog/delete/delete.component";
import { FormDialogComponent } from "./allpatients/dialog/form-dialog/form-dialog.component";
import { PatientService } from "./allpatients/patient.service";
import { SettingsPatientsComponent } from './settings-patients/settings-patients.component';
import {MatTabsModule} from "@angular/material/tabs";
import {TreatmentSettingsService} from "./settings-patients/settings/treatment/treatment.service";
import {MatBottomSheetModule} from "@angular/material/bottom-sheet";
import {NewTreatmentComponent} from "./settings-patients/settings/treatment/dialog/new-treatment/new-treatment";
import {TreatmentDeleteComponent} from "./settings-patients/settings/treatment/dialog/delete/delete.component";
import { TreatmentComponent } from './settings-patients/settings/treatment/treatment.component';
import { PlansComponent } from './settings-patients/settings/plans/plans.component';
import {PlanDeleteComponent} from "./settings-patients/settings/plans/dialog/delete/delete.component";
import {NewPlanComponent} from "./settings-patients/settings/plans/dialog/new-plan/new-plan";
import {PlanSettingsService} from "./settings-patients/settings/plans/plan.service";
import {NgxMaskModule} from "ngx-mask";

@NgModule({
  declarations: [
    AddPatientComponent,
    AllpatientsComponent,
    EditPatientComponent,
    PatientProfileComponent,
    DeleteComponent,
    FormDialogComponent,
    SettingsPatientsComponent,
    NewTreatmentComponent,
    TreatmentDeleteComponent,
    TreatmentComponent,
    PlansComponent,
    PlanDeleteComponent,
    NewPlanComponent
  ],
    imports: [
        CommonModule,
        PatientsRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MatTableModule,
        MatPaginatorModule,
        MatFormFieldModule,
        MatInputModule,
        MatSnackBarModule,
        MatButtonModule,
        MatIconModule,
        MatDialogModule,
        MatSortModule,
        MatToolbarModule,
        MatDatepickerModule,
        MatSelectModule,
        MatRadioModule,
        MatMenuModule,
        MatCheckboxModule,
        MaterialFileInputModule,
        MatProgressSpinnerModule,
        MatTabsModule,
        MatBottomSheetModule,
        NgxMaskModule
    ],
  providers: [PatientService,TreatmentSettingsService,PlanSettingsService],
})
export class PatientsModule {}
