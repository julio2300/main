export class Patient {
  pk_paciente: string | null;
  paciente_imagen: string;
  paciente_cedula: string;
  paciente_nombres: string;
  paciente_apellidos: string;
  paciente_correo: string;
  paciente_direccion: string;
  paciente_celular: string;
  paciente_fecha: string;
  paciente_fechanac: string;
  paciente_edad: string;
  paciente_grupo: string;
  paciente_genero: string;
  tratamiento_descri: string;
  fk_tratamiento: number;
  fk_plan: number;
  plan: any;
  representantes: any
  constructor(patient) {
    {
      this.pk_paciente = patient.pk_paciente || "";
      this.paciente_imagen =
        patient.paciente_imagen || "assets/images/user/user1.jpg";
      this.paciente_cedula = patient.paciente_cedula || "";
      this.paciente_nombres = patient.paciente_nombres || "";
      this.paciente_apellidos = patient.paciente_apellidos || "";
      this.paciente_correo = patient.paciente_correo || "";
      this.paciente_direccion = patient.paciente_direccion || "";
      this.paciente_celular = patient.paciente_celular || "";
      this.paciente_fecha = patient.paciente_fecha || "";
      this.paciente_fechanac = patient.paciente_fechanac || "";
      this.paciente_edad = patient.paciente_edad || "";
      this.paciente_grupo = patient.paciente_grupo || "";
      this.paciente_genero = patient.paciente_genero || "";
      this.tratamiento_descri = patient.tratamiento_descri || "";
      this.fk_tratamiento = patient.fk_tratamiento || null;
      this.fk_plan = patient.fk_plan || null;
      this.plan = patient.plan || null;
      this.representantes = patient.representantes || null;
    }
  }
  public getRandomID(): string {
    var S4 = function () {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return S4() + S4();
  }
}
export class DataModelPaginator {
  length: number;
  pageIndex: number;
  pageSize: number;
  previousPageIndex: number;
}
