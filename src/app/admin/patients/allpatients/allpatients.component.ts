import { AfterViewInit, Component, ElementRef, OnInit, ViewChild, } from "@angular/core";
import { PatientService } from "./patient.service";
import { HttpClient } from "@angular/common/http";
import { MatDialog } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { Patient } from "./patient.model";
import { DataModelSelect } from 'src/app/shared/DataModelSelect';
import { DataSource, SelectionModel } from "@angular/cdk/collections";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormDialogComponent } from "./dialog/form-dialog/form-dialog.component";
import { DeleteComponent } from "./dialog/delete/delete.component";
import { debounceTime, fromEvent, merge, Observable, } from "rxjs";
import { map } from "rxjs/operators";
import { UnsubscribeOnDestroyAdapter } from "src/app/shared/UnsubscribeOnDestroyAdapter";
import { AdminSettingsService } from "src/app/services/AdminSettings.service";
import { TreatmentSettingsService } from "../settings-patients/settings/treatment/treatment.service";
import { PlanSettingsService } from "../settings-patients/settings/plans/plan.service";
import { PlanModel } from "../settings-patients/settings/plans/Plan.Model";

@Component({
  selector: "app-allpatients",
  templateUrl: "./allpatients.component.html",
  styleUrls: ["./allpatients.component.sass"],
})
export class AllpatientsComponent extends UnsubscribeOnDestroyAdapter implements OnInit, AfterViewInit {
  displayedColumns = [
    "select",
    "img",
    "paciente_nombres",
    "paciente_apellidos",
    "paciente_cedula",
    "paciente_celular",
    "paciente_fechanac",
    "paciente_grupo",
    "tratamiento_descri",
    "actions",
  ];
  _patientService: PatientService | null;
  dataSource: DataSourcePatient | null;
  selection = new SelectionModel<Patient>(true, []);
  index: number;
  id: string;
  patient: Patient | null;
  dataModel: DataModelSelect = {
    limit: 10,
    offset: 0,
    count: 0,
    pageIndex: 0,
    opcion: "C",
    search: null,
  };
  source: any;
  SearchDataServe: string;
  btnLoadingDelete: boolean = false;

  valoresTratamientos: any;
  valoresPlan: PlanModel[];
  constructor(
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public patientService: PatientService,
    private snackBar: MatSnackBar,
    private _AdminSettingsService: AdminSettingsService,
    public tratamientos: TreatmentSettingsService,
    private plan: PlanSettingsService,
  ) {
    super();

    let opcion = "C";
    this.tratamientos
      .CrudTreatment({ limit: 1000, offset: 0 }, opcion)
      .subscribe((resp: any) => {
        this.valoresTratamientos = (resp.data.data);
      });
    this.plan
      .CrudPlan({ limit: 1000, offset: 0 }, opcion)
      .subscribe((resp: any) => {
        this.valoresPlan = (resp.data.data);
      });

  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild("filter", { static: true }) filter: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  ngAfterViewInit(): void {
    this.source = fromEvent(this.filter.nativeElement, "keyup");
    this.source.pipe(debounceTime(500)).subscribe((c) => {
      this.dataModel.opcion = "S";
      !this.SearchDataServe?.length && (this.dataModel.opcion = "C");
      this.dataModel.search = this.SearchDataServe;
      this.dataModel.limit = 10;
      this.dataModel.offset = 0;
      this.dataModel.count = 0;
      this.dataModel.pageIndex = 0;
      this.loadData();
    });
  }

  refresh() {
    this.loadData();
  }

  addNew() {
    let tempDirection;
    if (localStorage.getItem("isRtl") === "true") {
      tempDirection = "rtl";
    } else {
      tempDirection = "ltr";
    }
    const dialogRef = this.dialog.open(FormDialogComponent, {
      data: {
        patient: this.patient,
        action: "add",
        tratamiento: this.valoresTratamientos,
        plan: this.valoresPlan
      },
      width: "1000px",
      direction: tempDirection,
    });
    this.subs.sink = dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        this.refreshTable();
        this.showNotification(
          "snackbar-success",
          "Producto agregado con éxito...!!!",
          "bottom",
          "center"
        );
      }
    });
  }

  editCall(row) {
    this.id = row.pk_paciente;
    let tempDirection;
    if (localStorage.getItem("isRtl") === "true") {
      tempDirection = "rtl";
    } else {
      tempDirection = "ltr";
    }
    const dialogRef = this.dialog.open(FormDialogComponent, {
      data: {
        patient: row,
        action: "edit",
        tratamiento: this.valoresTratamientos,
        plan: this.valoresPlan
      },
      direction: tempDirection,
    });
    this.subs.sink = dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        // When using an edit things are little different, firstly we find record inside DataService by id
        const foundIndex = this._patientService.dataChange.value.findIndex(
          (x) => x.pk_paciente === this.id
        );
        // Then you update that record using data from dialogData (values you enetered)
        let data = (this._patientService.dataChange.value[foundIndex] =
          this.patientService.getDialogData());

        // And lastly refresh table
        this.refreshTable();
        this.showNotification(
          "black",
          "Registro actualizado con éxito..!!!",
          "bottom",
          "center"
        );
      }
    });
  }

  deleteItem(i: number, row) {
    this.index = i;
    this.id = row.pk_paciente;
    let tempDirection;
    if (localStorage.getItem("isRtl") === "true") {
      tempDirection = "rtl";
    } else {
      tempDirection = "ltr";
    }
    const dialogRef = this.dialog.open(DeleteComponent, {
      data: row,
      direction: tempDirection,
    });
    this.subs.sink = dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        const foundIndex = this._patientService.dataChange.value.findIndex(
          (x) => x.pk_paciente === this.id
        );
        // for delete we use splice in order to remove single object from DataService
        this._patientService.dataChange.value.splice(foundIndex, 1);
        this.refreshTable();
        this.showNotification(
          "snackbar-danger",
          "Registro eliminado con éxito...!!!",
          "bottom",
          "center"
        );
      }
    });
  }

  private refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.renderedData.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.renderedData.forEach((row) =>
        this.selection.select(row)
      );
  }

  removeSelectedRows() {
    this.btnLoadingDelete = true;
    let rowSelected = this.selection.selected.map(_ => _.pk_paciente);
    let data = this._patientService.dataChange.value;
    data = data.filter(_ => rowSelected.some(row => row === _.pk_paciente));
    const totalSelect = this.selection.selected.length;
    this.patientService
      .crudPaciente({ detalle: data }, "D")
      .subscribe((resp) => {
        this.btnLoadingDelete = false;
        this.refreshTable();
        this.selection = new SelectionModel<Patient>(true, []);
      });
    this.showNotification(
      "snackbar-danger",
      totalSelect + " Registros eliminado con éxito...!!!",
      "bottom",
      "center"
    );
  }

  public loadData() {
    this._patientService = new PatientService(
      this.httpClient,
      this._AdminSettingsService
    );
    this.dataSource = new DataSourcePatient(
      this._patientService,
      this.paginator,
      this.sort,
      this.dataModel
    );
  }

  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, "", {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

  getData(page) {
    let { length, pageIndex, pageSize, previousPageIndex } = page;
    this.dataModel.limit = pageSize;
    this.dataModel.pageIndex = pageIndex;
    this.dataModel.offset = pageIndex * this.dataModel.limit;
    this.loadData();
  }
}

export class DataSourcePatient extends DataSource<Patient> {
  filteredData: Patient[] = [];
  renderedData: Patient[] = [];

  constructor(
    public _patientService: PatientService,
    public paginator: MatPaginator,
    public _sort: MatSort,
    public dataModel: DataModelSelect
  ) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Patient[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._patientService.dataChange,
      this._sort.sortChange,
      this.paginator.page,
    ];
    // this._patientService.getAllPatients();

    this._patientService
      .crudPaciente(this.dataModel, this.dataModel.opcion)
      .subscribe((resp) => {
        // this.dataModel.count = Math.ceil(
        //   resp.data.conteo / this.dataModel.limit
        // );
        this.dataModel.count = resp.data.conteo;
      });
    return merge(...displayDataChanges).pipe(
      map(() => {
        // Filter data
        this.filteredData = this._patientService.data;
        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());
        return (this.renderedData = sortedData);
      })
    );
  }

  disconnect() {
  }

  /** Returns a sorted copy of the database data. */
  sortData(data: Patient[]): Patient[] {
    if (!this._sort.active || this._sort.direction === "") {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = "";
      let propertyB: number | string = "";
      switch (this._sort.active) {
        case "id":
          [propertyA, propertyB] = [a.pk_paciente, b.pk_paciente];
          break;
        case "name":
          [propertyA, propertyB] = [a.paciente_apellidos, b.paciente_apellidos];
          break;
        case "gender":
          [propertyA, propertyB] = [a.paciente_nombres, b.paciente_nombres];
          break;
        case "date":
          [propertyA, propertyB] = [a.paciente_fechanac, b.paciente_fechanac];
          break;
        case "address":
          [propertyA, propertyB] = [a.paciente_direccion, b.paciente_direccion];
          break;
        case "mobile":
          [propertyA, propertyB] = [a.paciente_celular, b.paciente_celular];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === "asc" ? 1 : -1)
      );
    });
  }
}
