import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { Component, Inject } from "@angular/core";
import { PatientService } from "../../patient.service";
import { TreatmentSettingsService } from "../../../settings-patients/settings/treatment/treatment.service";
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { Patient } from "../../patient.model";
import { PlanSettingsService } from "../../../settings-patients/settings/plans/plan.service";
import { PlanModel } from "../../../settings-patients/settings/plans/Plan.Model";

@Component({
  selector: "app-form-dialog",
  templateUrl: "./form-dialog.component.html",
  styleUrls: ["./form-dialog.component.sass"],
})
export class FormDialogComponent {
  action: string;
  dialogTitle: string;
  patientForm: FormGroup;
  valoresTratamientos: any;
  valoresPlan: PlanModel[];
  patient: Patient;
  loading: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<FormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public patientService: PatientService,
    private fb: FormBuilder
  ) {
    // Set the defaults
    this.action = data.action;
    if (this.action === "edit") {
      this.dialogTitle = data.patient.name;
      this.patient = data.patient;
    } else {
      this.dialogTitle = "Nuevo paciente";
      this.patient = new Patient({});
    }
    this.valoresPlan = this.data.plan;
    this.valoresTratamientos = this.data.tratamiento;

    this.patientForm = this.createContactForm();
  }
  formControl = new FormControl("", [
    Validators.required,
    // Validators.email,
  ]);
  getErrorMessage() {
    return this.formControl.hasError("required")
      ? "Required field"
      : this.formControl.hasError("email")
        ? "Not a valid email"
        : "";
  }
  createContactForm(): FormGroup {

    return this.fb.group({
      pk_paciente: [this.patient.pk_paciente],
      paciente_imagen: [this.patient.paciente_imagen],
      paciente_cedula: [this.patient.paciente_cedula],
      paciente_nombres: [this.patient.paciente_nombres],
      paciente_apellidos: [this.patient.paciente_apellidos],
      paciente_correo: [this.patient.paciente_correo],
      paciente_direccion: [this.patient.paciente_direccion],
      paciente_celular: [this.patient.paciente_celular],
      paciente_fecha: [this.patient.paciente_fecha],
      paciente_fechanac: [this.patient.paciente_fechanac],
      paciente_edad: [this.patient.paciente_edad],
      paciente_genero: [this.patient.paciente_genero],
      paciente_grupo: [this.patient.paciente_grupo],
      fk_tratamiento: [this.patient.fk_tratamiento],
      fk_plan: [this.patient.fk_plan],
    });
  }
  submit() {
    // emppty stuff
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  public confirmAdd(): void {
    // this.patientService.addPatient(this.patientForm.getRawValue());
    this.loading = true;
    let opcion = this.action === "edit" ? "U" : "I";
    let datos = this.action === "edit" ? this.patientForm.getRawValue() : { detalle: [this.patientForm.getRawValue()] };
    this.patientService.crudPaciente(datos, opcion).subscribe((resp: any) => {
      this.dialogRef.close(1);
      this.loading = false;
    });
  }
}
