import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsPatientsComponent } from './settings-patients.component';

describe('SettingsPatientsComponent', () => {
  let component: SettingsPatientsComponent;
  let fixture: ComponentFixture<SettingsPatientsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsPatientsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsPatientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
