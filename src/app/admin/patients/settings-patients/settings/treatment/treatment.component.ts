import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {UnsubscribeOnDestroyAdapter} from "../../../../../shared/UnsubscribeOnDestroyAdapter";
import {TreatmentModel} from "./Treatment.Model";
import {DataSource, SelectionModel} from "@angular/cdk/collections";
import {DataModelSelect} from "../../../../../shared/DataModelSelect";
import {TreatmentSettingsService} from "./treatment.service";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {FormBuilder} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {AdminSettingsService} from "../../../../../services/AdminSettings.service";
import {MatBottomSheet} from "@angular/material/bottom-sheet";
import {debounceTime, fromEvent, merge, Observable} from "rxjs";
import {NewTreatmentComponent} from "./dialog/new-treatment/new-treatment";
import {TreatmentDeleteComponent} from "./dialog/delete/delete.component";
import {map} from "rxjs/operators";

@Component({
  selector: 'settings-treatment',
  templateUrl: './treatment.component.html',
  styleUrls: ['./treatment.component.sass']
})
export class TreatmentComponent extends UnsubscribeOnDestroyAdapter implements OnInit, AfterViewInit {

  Treatment: TreatmentModel | null;
  loading: boolean = false;
  //Variables for  DataTable
  dataSource: DataSourcespecialties | null;
  displayedColumns = [
    "select",
    "img",
    "tratamiento_descri",
    "tratamiento_fecha",
    "actions",
  ];
  selection = new SelectionModel<TreatmentModel>(true, []);
  source: any;
  SearchDataServe: string;
  dataModel: DataModelSelect = {
    limit: 10,
    offset: 0,
    count: 0,
    pageIndex: 0,
    opcion: "C",
    search: null,
  };
  _TreatmentServices: TreatmentSettingsService | null;
  btnLoadingDelete: boolean = false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild("filter", {static: true}) filter: ElementRef;
  id: number;
  index: number;
  title = 'Crear';
  // _SpecialtiesService: TreatmentSettingsService | null;
  //End Variables DataTable
  constructor(
    private fb: FormBuilder,
    public httpClient: HttpClient,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private _AdminSettingsService: AdminSettingsService,
    private SpecialtiesService: TreatmentSettingsService,
    private _bottomSheet: MatBottomSheet
  ) {
    super();
    this.Treatment = new TreatmentModel({});
  }

  ngOnInit(): void {
    this.loadData();
  }

  //Functions for DataTable
  ngAfterViewInit(): void {
    this.source = fromEvent(this.filter.nativeElement, "keyup");
    this.source.pipe(debounceTime(500)).subscribe((c) => {
      this.dataModel.opcion = "S";
      !this.SearchDataServe?.length && (this.dataModel.opcion = "C");
      this.dataModel.search = this.SearchDataServe;
      this.dataModel.limit = 10;
      this.dataModel.offset = 0;
      this.dataModel.count = 0;
      this.dataModel.pageIndex = 0;
      this.loadData();
    });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.renderedData.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.renderedData.forEach((row) =>
        this.selection.select(row)
      );
  }

  public loadData() {
    this._TreatmentServices = new TreatmentSettingsService(
      this.httpClient,
      this._AdminSettingsService
    );
    this.newDataRenove();
  }

  newDataRenove() {
    this.dataSource = new DataSourcespecialties(
      this._TreatmentServices,
      this.paginator,
      this.sort,
      this.dataModel
    );

  }

  getData(page) {
    let {length, pageIndex, pageSize, previousPageIndex} = page;
    this.dataModel.limit = pageSize;
    this.dataModel.pageIndex = pageIndex;
    this.dataModel.offset = pageIndex * this.dataModel.limit;
    this.loadData();
  }

  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, "", {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

  removeSelectedRows() {
    this.btnLoadingDelete = true;
    let rowSelected = this.selection.selected.map(_ => _.pk_tratamiento);
    let data = this._TreatmentServices.dataChange.value;
    data = data.filter(_ => rowSelected.some(row => row === _.pk_tratamiento));
    const totalSelect = this.selection.selected.length;
    this.SpecialtiesService
      .CrudTreatment({detalle: data}, "D")
      .subscribe((resp) => {
        this.btnLoadingDelete = false;
        this.refresh();
        this.selection = new SelectionModel<TreatmentModel>(true, []);
        this.showNotification(
          "snackbar-danger",
          totalSelect + " registros fueron eliminado con éxito...!!!",
          "bottom",
          "center"
        );
        resp.data.code === 500 && this.showNotification(
          "snackbar-danger",
          "Error, tratamiento vinculado con pacientes.",
          "top",
          "rigth"
        );
      });
  }

  private refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }

  refresh() {
    this.loadData();
  }

  addNew() {
    const data = this._bottomSheet.open(NewTreatmentComponent, {data: {action: "Crear"}});
    data.afterDismissed().subscribe((result) => {
      result && this.refresh();
    });
  }

  editCall(row: TreatmentModel) {
    // this.title = 'Modificar';
    this.Treatment.pk_tratamiento = row.pk_tratamiento;
    this.Treatment.tratamiento_descri = row.tratamiento_descri;
    const data = this._bottomSheet.open(NewTreatmentComponent, {
      data: {
        action: "Modificar",
        Treatment: this.Treatment
      }
    });
    data.afterDismissed().subscribe((result) => {
      result && this.refresh();
    });
    // this.doctorsEspe = this.createDataForm();
  }

  deleteItem(i: number, row: TreatmentModel) {
    this.index = i;
    this.id = row.pk_tratamiento;
    let tempDirection;
    if (localStorage.getItem("isRtl") === "true") {
      tempDirection = "rtl";
    } else {
      tempDirection = "ltr";
    }
    const dialogRef = this.dialog.open(TreatmentDeleteComponent, {
      data: [row],
      direction: tempDirection,
    });
    this.subs.sink = dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        const foundIndex = this._TreatmentServices.dataChange.value.findIndex(
          (x) => x.pk_tratamiento === this.id
        );
        // for delete we use splice in order to remove single object from DataService
        this._TreatmentServices.dataChange.value.splice(foundIndex, 1);
        this.refreshTable();
        this.showNotification(
          "snackbar-danger",
          "Registro eliminado con éxito...!!!",
          "bottom",
          "center"
        );
      }
    });
  }

  //End Functions DataTable
}

//Data Server get for Especialidades
export class DataSourcespecialties extends DataSource<TreatmentModel> {
  filteredData: TreatmentModel[] = [];
  renderedData: TreatmentModel[] = [];

  constructor(
    public _specialtiesService: TreatmentSettingsService,
    public paginator: MatPaginator,
    public _sort: MatSort,
    public dataModel: DataModelSelect
  ) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<TreatmentModel[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._specialtiesService.dataChange,
      this._sort.sortChange,
      this.paginator.page,
    ];
    // this._specialtiesService.getAllPatients();
    this._specialtiesService
      .CrudTreatment(this.dataModel, this.dataModel.opcion)
      .subscribe((resp) => {
        this.dataModel.count = Math.ceil(
          resp.data.conteo / this.dataModel.limit
        );
      });
    return merge(...displayDataChanges).pipe(
      map(() => {
        // Filter data
        this.filteredData = this._specialtiesService.data;
        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());
        return (this.renderedData = sortedData);
      })
    );
  }

  disconnect() {
  }

  /** Returns a sorted copy of the database data. */
  sortData(data: TreatmentModel[]): TreatmentModel[] {
    if (!this._sort.active || this._sort.direction === "") {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = "";
      let propertyB: number | string = "";
      switch (this._sort.active) {
        case "pk_tratamiento":
          [propertyA, propertyB] = [a.pk_tratamiento, b.pk_tratamiento];
          break;
        case "trat_mame":
          [propertyA, propertyB] = [a.tratamiento_descri, b.tratamiento_descri];
          break;
        case "tratamiento_fecha":
          [propertyA, propertyB] = [new Date(a.tratamiento_fecha).getTime(), new Date(b.tratamiento_fecha).getTime()];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === "asc" ? 1 : -1)
      );
    });
  }
}
