import {Component, Inject} from "@angular/core";
import {FormBuilder, FormGroup} from "@angular/forms";
import {TreatmentModel} from "../../Treatment.Model";
import {TreatmentSettingsService} from "../../treatment.service";
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from "@angular/material/bottom-sheet";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'new-treatment',
  templateUrl: 'new-treatment.html',
})
export class NewTreatmentComponent {
  doctorsEspe: FormGroup;
  title: string = 'Crear';
  Treatment: TreatmentModel | null;
  loading: boolean = false;
  _TreatmentServices: TreatmentSettingsService | null;

  constructor(
    private fb: FormBuilder,
    private _bottomSheetRef: MatBottomSheetRef<NewTreatmentComponent>,
    private snackBar: MatSnackBar,
    public _specialtiesService: TreatmentSettingsService,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
  ) {
    this.title=this.data.action;
    this.Treatment = this.data.Treatment || new TreatmentModel({});
    this.doctorsEspe = this.createDataForm();
  }

  openLink(event: MouseEvent): void {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }

  createDataForm(): FormGroup {
    return this.fb.group({
      pk_tratamiento: [this.Treatment.pk_tratamiento],
      tratamiento_descri: [this.Treatment.tratamiento_descri],
      tratamiento_fecha: [this.Treatment.tratamiento_fecha]
    });
  }

  submit() {
    if (this.title == 'Crear') {
      this.SendDataServerInsert();
    } else {
      this.SendDataServerUpdate();
    }
  }

  SendDataServerInsert() {
    this.loading = true;
    let datos = this.doctorsEspe.getRawValue();
    this._specialtiesService.CrudTreatment({detalle: [datos]}, 'I').subscribe(resp => {
      this.loading = false;
      this.showNotification(
        "snackbar-success",
        "Especialidad agregada con éxito...!!!",
        "bottom",
        "center"
      );
      this._bottomSheetRef.dismiss(true);
      this.doctorsEspe.reset();
    });
  }

  SendDataServerUpdate() {
    this.loading = true;
    this._specialtiesService.CrudTreatment(this.doctorsEspe.getRawValue(), 'U').subscribe(resp => {
      this.loading = false;
      this.showNotification(
        "snackbar-success",
        "Especialidad editada con éxito...!!!",
        "bottom",
        "center"
      );
      this._bottomSheetRef.dismiss(true);
    });
  }
  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, "", {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }
}
