import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { Component, Inject } from "@angular/core";
import { TreatmentSettingsService } from "../../treatment.service";
import {TreatmentModel} from "../../Treatment.Model";

@Component({
  selector: "app-delete",
  templateUrl: "./delete.component.html",
  styleUrls: ["./delete.component.sass"],
})
export class TreatmentDeleteComponent {
  loading: boolean = false;
  constructor(
    public dialogRef: MatDialogRef<TreatmentDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TreatmentModel[],
    public _TreatmentService: TreatmentSettingsService
  ) {}
  onNoClick(): void {
    this.dialogRef.close();
  }
  confirmDelete(): void {
    // this._TreatmentService.deletePatient(this.data.pk_paciente);
    this.loading = true;
    this._TreatmentService
      .CrudTreatment({ detalle: this.data }, "D")
      .subscribe((resp) => {
        this.loading = false;
        this.dialogRef.close(1);
      });
  }
}
