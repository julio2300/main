export class TreatmentModel {
  pk_tratamiento:number;
  tratamiento_descri:string;
  tratamiento_fecha:Date;
  constructor(especialidad) {
    this.pk_tratamiento=especialidad.pk_espe || 0;
    this.tratamiento_descri=especialidad.tratamiento_descri||'';
    this.tratamiento_fecha=especialidad.tratamiento_fecha||new Date().toISOString();
  }
}
