import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {PlanModel} from "./Plan.Model";
import {DataSource, SelectionModel} from "@angular/cdk/collections";
import {DataModelSelect} from "../../../../../shared/DataModelSelect";
import {PlanSettingsService} from "./plan.service";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {FormBuilder} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {AdminSettingsService} from "../../../../../services/AdminSettings.service";
import {MatBottomSheet} from "@angular/material/bottom-sheet";
import {debounceTime, fromEvent, merge, Observable} from "rxjs";
import {NewPlanComponent} from "./dialog/new-plan/new-plan";
import {PlanDeleteComponent} from "./dialog/delete/delete.component";
import {map} from "rxjs/operators";
import {UnsubscribeOnDestroyAdapter} from "../../../../../shared/UnsubscribeOnDestroyAdapter";

@Component({
  selector: 'settings-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.sass']
})
export class PlansComponent extends UnsubscribeOnDestroyAdapter implements OnInit, AfterViewInit {

  Plan: PlanModel | null;
  loading: boolean = false;
  //Variables for  DataTable
  dataSource: DataSourcespecialties | null;
  displayedColumns = [
    "select",
    "img",
    "plan_descri",
    "plan_valor",
    "plan_fecha",
    "plan_desc",
    "plan_desc_meses",
    "actions",
  ];
  selection = new SelectionModel<PlanModel>(true, []);
  source: any;
  SearchDataServe: string;
  dataModel: DataModelSelect = {
    limit: 10,
    offset: 0,
    count: 0,
    pageIndex: 0,
    opcion: "C",
    search: null,
  };
  _PlanServices: PlanSettingsService | null;
  btnLoadingDelete: boolean = false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild("filter", {static: true}) filter: ElementRef;
  id: number;
  index: number;
  title = 'Crear';
  // _SpecialtiesService: PlanSettingsService | null;
  //End Variables DataTable
  constructor(
    private fb: FormBuilder,
    public httpClient: HttpClient,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private _AdminSettingsService: AdminSettingsService,
    private SpecialtiesService: PlanSettingsService,
    private _bottomSheet: MatBottomSheet
  ) {
    super();
    this.Plan = new PlanModel({});
  }

  ngOnInit(): void {
    this.loadData();
  }

  //Functions for DataTable
  ngAfterViewInit(): void {
    this.source = fromEvent(this.filter.nativeElement, "keyup");
    this.source.pipe(debounceTime(500)).subscribe((c) => {
      this.dataModel.opcion = "S";
      !this.SearchDataServe?.length && (this.dataModel.opcion = "C");
      this.dataModel.search = this.SearchDataServe;
      this.dataModel.limit = 10;
      this.dataModel.offset = 0;
      this.dataModel.count = 0;
      this.dataModel.pageIndex = 0;
      this.loadData();
    });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.renderedData.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.renderedData.forEach((row) =>
        this.selection.select(row)
      );
  }

  public loadData() {
    this._PlanServices = new PlanSettingsService(
      this.httpClient,
      this._AdminSettingsService
    );
    this.newDataRenove();
  }

  newDataRenove() {
    this.dataSource = new DataSourcespecialties(
      this._PlanServices,
      this.paginator,
      this.sort,
      this.dataModel
    );

  }

  getData(page) {
    let {length, pageIndex, pageSize, previousPageIndex} = page;
    this.dataModel.limit = pageSize;
    this.dataModel.pageIndex = pageIndex;
    this.dataModel.offset = pageIndex * this.dataModel.limit;
    this.loadData();
  }

  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, "", {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

  removeSelectedRows() {
    this.btnLoadingDelete = true;
    let rowSelected = this.selection.selected.map(_ => _.pk_plan);
    let data = this._PlanServices.dataChange.value;
    data = data.filter(_ => rowSelected.some(row => row === _.pk_plan));
    const totalSelect = this.selection.selected.length;
    this.SpecialtiesService
      .CrudPlan({detalle: data}, "D")
      .subscribe((resp) => {
        this.btnLoadingDelete = false;
        this.refresh();
        this.selection = new SelectionModel<PlanModel>(true, []);
        this.showNotification(
          "snackbar-danger",
          totalSelect + " registros fueron eliminado con éxito...!!!",
          "bottom",
          "center"
        );
        resp.data.code === 500 && this.showNotification(
          "snackbar-danger",
          "Error, tratamiento vinculado con pacientes.",
          "top",
          "rigth"
        );
      });
  }

  private refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }

  refresh() {
    this.loadData();
  }

  addNew() {
    const data = this._bottomSheet.open(NewPlanComponent, {data: {action: "Crear"}});
    data.afterDismissed().subscribe((result) => {
      result && this.refresh();
    });
  }

  editCall(row: PlanModel) {
    // this.title = 'Modificar';
    // this.Plan.pk_plan = row.pk_plan;
    // this.Plan.plan_descri = row.plan_descri;
    // this.Plan.plan_valor = row.plan_valor;
    this.Plan = row;
    const data = this._bottomSheet.open(NewPlanComponent, {
      data: {
        action: "Modificar",
        Plan: this.Plan
      }
    });
    data.afterDismissed().subscribe((result) => {
      result && this.refresh();
    });
    // this.doctorsEspe = this.createDataForm();
  }

  deleteItem(i: number, row: PlanModel) {
    this.index = i;
    this.id = row.pk_plan;
    let tempDirection;
    if (localStorage.getItem("isRtl") === "true") {
      tempDirection = "rtl";
    } else {
      tempDirection = "ltr";
    }
    const dialogRef = this.dialog.open(PlanDeleteComponent, {
      data: [row],
      direction: tempDirection,
    });
    this.subs.sink = dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        const foundIndex = this._PlanServices.dataChange.value.findIndex(
          (x) => x.pk_plan === this.id
        );
        // for delete we use splice in order to remove single object from DataService
        this._PlanServices.dataChange.value.splice(foundIndex, 1);
        this.refreshTable();
        this.showNotification(
          "snackbar-danger",
          "Registro eliminado con éxito...!!!",
          "bottom",
          "center"
        );
      }
    });
  }

  //End Functions DataTable
}

//Data Server get for Especialidades
export class DataSourcespecialties extends DataSource<PlanModel> {
  filteredData: PlanModel[] = [];
  renderedData: PlanModel[] = [];

  constructor(
    public _specialtiesService: PlanSettingsService,
    public paginator: MatPaginator,
    public _sort: MatSort,
    public dataModel: DataModelSelect
  ) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<PlanModel[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._specialtiesService.dataChange,
      this._sort.sortChange,
      this.paginator.page,
    ];
    // this._specialtiesService.getAllPatients();
    this._specialtiesService
      .CrudPlan(this.dataModel, this.dataModel.opcion)
      .subscribe((resp) => {
        this.dataModel.count = Math.ceil(
          resp.data.conteo / this.dataModel.limit
        );
      });
    return merge(...displayDataChanges).pipe(
      map(() => {
        // Filter data
        this.filteredData = this._specialtiesService.data;
        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());
        return (this.renderedData = sortedData);
      })
    );
  }

  disconnect() {
  }

  /** Returns a sorted copy of the database data. */
  sortData(data: PlanModel[]): PlanModel[] {
    if (!this._sort.active || this._sort.direction === "") {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = "";
      let propertyB: number | string = "";
      switch (this._sort.active) {
        case "pk_plan":
          [propertyA, propertyB] = [a.pk_plan, b.pk_plan];
          break;
        case "plan_descri":
          [propertyA, propertyB] = [a.plan_descri, b.plan_descri];
          break;
        case "plan_valor":
          [propertyA, propertyB] = [a.plan_valor, b.plan_valor];
          break;
        case "plan_desc":
          [propertyA, propertyB] = [a.plan_desc, b.plan_desc];
          break;
        case "plan_desc_meses":
          [propertyA, propertyB] = [a.plan_desc_meses, b.plan_desc_meses];
          break;
        case "tratamiento_fecha":
          [propertyA, propertyB] = [new Date(a.plan_fecha).getTime(), new Date(b.plan_fecha).getTime()];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === "asc" ? 1 : -1)
      );
    });
  }
}

