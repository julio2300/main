export class PlanModel {
  pk_plan: number;
  plan_descri: string;
  plan_valor: number;
  plan_fecha: Date;
  plan_desc: number;
  plan_desc_meses: number;

  constructor(plan) {
    this.pk_plan = plan.pk_plan || 0;
    this.plan_descri = plan.plan_descri || '';
    this.plan_valor = plan.plan_valor;
    this.plan_fecha = plan.plan_fecha || new Date().toISOString();
    this.plan_desc = plan.plan_desc || 0;
    this.plan_desc_meses = plan.plan_desc_meses || 0;
  }
}
