import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { Component, Inject } from "@angular/core";
import { PlanSettingsService } from "../../plan.service";
import {PlanModel} from "../../Plan.Model";

@Component({
  selector: "app-delete",
  templateUrl: "./delete.component.html",
  styleUrls: ["./delete.component.sass"],
})
export class PlanDeleteComponent {
  loading: boolean = false;
  constructor(
    public dialogRef: MatDialogRef<PlanDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: PlanModel[],
    public _PlanService: PlanSettingsService
  ) {}
  onNoClick(): void {
    this.dialogRef.close();
  }
  confirmDelete(): void {
    // this._PlanService.deletePatient(this.data.pk_paciente);
    this.loading = true;
    this._PlanService
      .CrudPlan({ detalle: this.data }, "D")
      .subscribe((resp) => {
        this.loading = false;
        this.dialogRef.close(1);
      });
  }
}
