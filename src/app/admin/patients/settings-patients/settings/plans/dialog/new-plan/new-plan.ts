import {Component, Inject} from "@angular/core";
import {FormBuilder, FormGroup} from "@angular/forms";
import {PlanModel} from "../../Plan.Model";
import {PlanSettingsService} from "../../plan.service";
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from "@angular/material/bottom-sheet";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'new-treatment',
  templateUrl: 'new-plan.html',
})
export class NewPlanComponent {
  doctorsEspe: FormGroup;
  title: string = 'Crear';
  Plan: PlanModel | null;
  loading: boolean = false;
  _PlanServices: PlanSettingsService | null;

  constructor(
    private fb: FormBuilder,
    private _bottomSheetRef: MatBottomSheetRef<NewPlanComponent>,
    private snackBar: MatSnackBar,
    public _specialtiesService: PlanSettingsService,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
  ) {
    this.title = this.data.action;
    this.Plan = this.data.Plan || new PlanModel({});
    this.doctorsEspe = this.createDataForm();
  }

  openLink(event: MouseEvent): void {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }

  createDataForm(): FormGroup {
    return this.fb.group({
      pk_plan: [this.Plan.pk_plan],
      plan_fecha: [this.Plan.plan_fecha],
      plan_descri: [this.Plan.plan_descri],
      plan_valor: [this.Plan.plan_valor],
      plan_desc: [this.Plan.plan_desc * 100],
      plan_desc_meses: [this.Plan.plan_desc_meses]
    });
  }

  submit() {
    if (this.title == 'Crear') {
      this.SendDataServerInsert();
    } else {
      this.SendDataServerUpdate();
    }
  }

  SendDataServerInsert() {
    this.loading = true;
    let datos: PlanModel = this.doctorsEspe.getRawValue();
    datos.plan_desc /= 100;
    this._specialtiesService.CrudPlan({detalle: [datos]}, 'I').subscribe(resp => {
      this.loading = false;
      this.showNotification(
        "snackbar-success",
        "Especialidad agregada con éxito...!!!",
        "bottom",
        "center"
      );
      this._bottomSheetRef.dismiss(true);
      this.doctorsEspe.reset();
    });
  }

  SendDataServerUpdate() {
    this.loading = true;
    let datos: PlanModel = this.doctorsEspe.getRawValue();
    datos.plan_desc /= 100;
    this._specialtiesService.CrudPlan(datos, 'U').subscribe(resp => {
      this.loading = false;
      this.showNotification(
        "snackbar-success",
        "Especialidad editada con éxito...!!!",
        "bottom",
        "center"
      );
      this._bottomSheetRef.dismiss(true);
    });
  }

  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, "", {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }
}
