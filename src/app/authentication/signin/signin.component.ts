import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "src/app/core/service/auth.service";
import { Role } from "src/app/core/models/role";
import { UnsubscribeOnDestroyAdapter } from "src/app/shared/UnsubscribeOnDestroyAdapter";

import { AdminSettingsService } from "src/app/services/AdminSettings.service";
import * as bcrypt from "bcryptjs";

@Component({
  selector: "app-signin",
  templateUrl: "./signin.component.html",
  styleUrls: ["./signin.component.scss"],
})
export class SigninComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  authForm: FormGroup;
  submitted = false;
  loading = false;
  error = "";
  hide = true;
  ModeloLogin: ModeloLogin = {
    user_email: null,
    user_pass: null,
  };
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private _AdminSettingsService: AdminSettingsService
  ) {
    super();
  }

  ngOnInit() {
    this.authForm = this.formBuilder.group({
      username: ["admin@hospital.org", Validators.required],
      password: ["admin@123", Validators.required],
    });
  }
  get f() {
    return this.authForm.controls;
  }
  adminSet() {
    this.authForm.get("username").setValue("admin@hospital.org");
    this.authForm.get("password").setValue("admin@123");
  }
  doctorSet() {
    this.authForm.get("username").setValue("doctor@hospital.org");
    this.authForm.get("password").setValue("doctor@123");
  }
  patientSet() {
    this.authForm.get("username").setValue("patient@hospital.org");
    this.authForm.get("password").setValue("patient@123");
  }
  onSubmit() {
    this.submitted = true;
    this.loading = true;
    this.error = "";
    if (this.authForm.invalid) {
      this.error = "Username and Password not valid !";
      return;
    } else {
      this.ModeloLogin = {
        user_email: this.f.username.value,
        user_pass: this.f.password.value,
      };

      this.subs.sink = this.authService.login("L", this.ModeloLogin).subscribe(
        // this.subs.sink = this.authService
        //   .login(this.f.username.value, this.f.password.value)
        // .subscribe(
        (res) => {
          if (res) {
            // setTimeout(() => {
            const role = this.authService.currentUserValue.role;
            console.log(role);

            if (role === Role.All || role === Role.Admin) {
              this.router.navigate(["/admin/dashboard/main"]);
            } else if (role === Role.Doctor) {
              this.router.navigate(["/doctor/dashboard"]);
            } else if (role === Role.Patient) {
              this.router.navigate(["/patient/dashboard"]);
            } else {
              this.router.navigate(["/authentication/signin"]);
            }
            this.loading = false;
            // }, 1000);
          } else {
            this.error = "Invalid Login";
          }
        },
        (error) => {
          this.error = error;
          this.submitted = false;
          this.loading = false;
        }
      );
    }
  }
}

export interface ModeloLogin {
  user_email: string;
  user_pass: string;
}
