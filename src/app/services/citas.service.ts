import { Injectable } from "@angular/core";
import { dominio_ws } from "../config/SettingsGlobal";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { catchError } from "rxjs/operators";
import { AdminSettingsService } from "./AdminSettings.service";
import { throwError } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class CitasService {
  url: string = dominio_ws;
  constructor(
    public http: HttpClient,
    private _AdminSettingsService: AdminSettingsService
  ) {}

  apiCategoria(opcion, pk_empresa: number, data: any) {
    this._AdminSettingsService.cargarValoresLocalStorage();
    let tkJwt = this._AdminSettingsService.MyData.token;
    let headers = new HttpHeaders();
    headers = headers.set("token", tkJwt);

    let QueryData = {
      data: this._AdminSettingsService.encryptData({
        opcion: opcion,
        pk_empresa: pk_empresa,
        json: data,
      }),
    };
    // console.log(query);

    return this.http
      .post(`${environment.apiServer}/1352`, QueryData, { headers })
      .pipe(
        map((resp: any) => {
          return this._AdminSettingsService.decryptData(resp.data);
        })
      )
      .pipe(
        catchError((err) => {
          console.log("Error no controlado " + JSON.stringify(err));
          return throwError(err);
        })
      );
  }
}
