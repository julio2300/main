import { Injectable } from "@angular/core";
import * as crypto from "crypto";

@Injectable({
  providedIn: "root",
})
export class CrytoService {
  constructor() {}

  generateKeys = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const { publicKey, privateKey } = crypto.generateKeyPairSync("rsa", {
          modulusLength: 4096,
          publicKeyEncoding: { type: "spki", format: "pem" },
          privateKeyEncoding: {
            type: "pkcs8",
            format: "pem",
            cipher: "aes-256-cbc",
            passphrase: "_A%q0m3DL#&56m1Fmk",
          },
        });
        console.log(privateKey, publicKey);

        resolve({ privateKey, publicKey });
      } catch (error) {
        reject(error);
      }
    });
  };
}
