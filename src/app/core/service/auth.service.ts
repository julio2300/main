import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable, of } from "rxjs";
import { map } from "rxjs/operators";
import { User } from "../models/user";
import { environment } from "src/environments/environment";
import { AdminSettingsService } from "src/app/services/AdminSettings.service";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(
    private http: HttpClient,
    private _AdminSettingsService: AdminSettingsService
  ) {
    let data = this._AdminSettingsService.decryptData(
      localStorage.getItem("DevCode")
    )?.data.data[0].datos;

    data = data ? data : null;

    this.currentUserSubject = new BehaviorSubject<User>(data);
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(opcion: string, data: any) {
    let QueryData = {
      data: this._AdminSettingsService.encryptData({
        opcion: opcion,
        json: data,
      }),
    };

    return this.http.post<any>(`${environment.apiServer}/1853`, QueryData).pipe(
      map((user) => {
        if (user.status == 200) {
          let data = this._AdminSettingsService.decryptData(user.data).data
            .data[0].datos;
          this.currentUserSubject.next(data);
          this._AdminSettingsService.limpiarTokenUser();
          this._AdminSettingsService.crearTokenUsuarioLocalStorage(user.data);
          return data;
        } else {
          return "Credenciales Incorrectas";
        }
      })
    );
  }

  login2(username: string, password: string) {
    return this.http
      .post<any>(`${environment.apiUrl}/authenticate`, {
        username,
        password,
      })
      .pipe(
        map((user) => {
          // store user details and jwt token in local storage to keep user logged in between page refreshes

          localStorage.setItem("DevCode", JSON.stringify(user));
          this.currentUserSubject.next(user);

          return user;
        })
      );
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem("DevCode");
    this.currentUserSubject.next(null);
    return of({ success: false });
  }
}
